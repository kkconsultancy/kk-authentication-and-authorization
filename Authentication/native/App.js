
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import Presentation from './src/components/signin/index.js';
import Register from './src/components/signup/index';
import ResetPassword from './src/components/resetpassword/index';
import Dashboard from './src/components/dashboard/index';
import DeleteAccount from './src/components/Delete Account/index'
import UpdateProfile from './src/components/UpdateProfile/index'
import DisableAccount from './src/components/DisableAccount/index'
// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,pm\n' +
//     'Shake or press menu button for dev menu',
// });


export default class App extends Component {
  render() {
    return (
    
        <Routes/>
        );
  }
}

const Routes =() => {
  return(
    <Router>
      <Scene>
      <Scene key = "home" component = {Presentation} title = "HOME" initial = {true} />
           <Scene key = "register" component = {Register} title = "SIGN UP" />
          <Scene key = "dashboard" component = {Dashboard} title = "DASHBOARD" />
          <Scene key = "resetpassword" component = {ResetPassword} title = "RESET PASSWORD" /> 
          <Scene key = "deleteaccount" component = {DeleteAccount} title = "DELETE ACCOUNT" />
          <Scene key = "updateprofile" component = {UpdateProfile} title = "UPDATE PROFILE" />
          <Scene key = "disableaccount" component = {DisableAccount} title = "DISABLE ACCOUNT" />
          
          </Scene>
    </Router>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
