import React from 'react';
import { View,TouchableOpacity,Text,StyleSheet} from 'react-native';
import ViewDashboard from './presentation.js';
import { Actions } from 'react-native-router-flux';

const Dashboard = () => {
    const goToHome = () => {
        Actions.home()
     }
    return (
        <View>
            <ViewDashboard/>
            <TouchableOpacity  style = {styles.submitButton}>
                    <Text style = {styles.submitButtonText} > CHANGE PROFILE </Text>
            </TouchableOpacity>
            <TouchableOpacity  onPress={goToHome} style = {styles.submitButton}>
                    <Text style = {styles.submitButtonText} > DELETE ACCOUNT </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={goToHome}  style = {styles.submitButton}>
                    <Text style = {styles.submitButtonText} > LOG OUT </Text>
            </TouchableOpacity>
        </View>
   )
}
export default Dashboard

const styles = StyleSheet.create({
    submitButton: {
       backgroundColor: '#7a42f4',
       padding: 10,
       margin: 15,
       height: 40,
    },
    submitButtonText:{
       color: 'white',
       textAlign : 'center'
    }
 })

