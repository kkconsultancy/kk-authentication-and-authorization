import React,{Component} from 'react'
import { TouchableOpacity,TextInput, Text, View, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Home = () => {
    const goToRegister = () => {
       Actions.register()
    }
    const goToResetPassword = () => {
        Actions.resetpassword()
    }
    const goToDashboard = () => {
        Actions.dashboard()
     }
    return (
        <View>
             <Login/>
             <TouchableOpacity onPress = {goToDashboard} style = {styles.submitButton}>
                  <Text style = {styles.submitButtonText}  > LOG IN </Text>
            </TouchableOpacity>
             <TouchableOpacity onPress = {goToResetPassword}>
                 <Text style={styles.textView}>Forgot Password?</Text>
             </TouchableOpacity>
             <TouchableOpacity onPress = {goToRegister}>
                 <Text style={styles.textView}>Don't you have any account? Sign Up</Text>
             </TouchableOpacity>
         </View>
    )
 }

 export default Home

class Login extends Component {
    state = {
        username:'',
        password:''
    }
    handleusername = (text) => {  
        this.setState({username:text});
    }
    handlepassword = (text) => {
        this.setState({password:text})
    }
    render() {
        return(
            <View style = {styles.container}>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Username"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleusername}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Password"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handlepassword}/>
                
            </View>
        )
    }
}
 
 const styles = StyleSheet.create({
     textView : {
         textAlign:'center',
         fontSize : 18,
         fontWeight: 'bold'
     },container: {
        paddingTop: 23
     },
     input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1,
        fontWeight:'bold'
     },
     submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
     },
     submitButtonText:{
        color: 'white',
        textAlign : 'center'
     }
 })