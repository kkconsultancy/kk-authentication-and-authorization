import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { AppRegistry} from 'react-native';
import Home from './presentation.js';
import Register from '../signup/index.js';
import ResetPassword from '../resetpassword/index.js';
import Dashboard from '../dashboard/index.js';

class AuthenticateApp extends Component {
   render() {
      return (
         <Routes />
      )
   }
}
export default AuthenticateApp
AppRegistry.registerComponent('AuthenticateApp', () => AuthenticateApp)

const Routes = () => (
    <Router>
       <Scene key = "root">
          <Scene key = "home" component = {Home} title = "HOME" initial = {true} />
          <Scene key = "register" component = {Register} title = "SIGN UP" />
          <Scene key = "dashboard" component = {Dashboard} title = "DASHBOARD" />
          <Scene key = "resetpassword" component = {ResetPassword} title = "RESET PASSWORD" />
       </Scene>
    </Router>
 )