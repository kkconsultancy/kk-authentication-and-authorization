import React from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import Inputs from './presentation.js';
import { Actions } from 'react-native-router-flux';

const Register = () => {
    const goToHome = () => {
        Actions.home()
     }
    return (
        <View>
            <Inputs/>
            <TouchableOpacity onPress = {goToHome} style = {styles.submitButton}>
                <Text style = {styles.submitButtonText} > REGISTER </Text>
            </TouchableOpacity>
        </View>
   )
}
export default Register

const styles = StyleSheet.create({
    submitButton: {
       backgroundColor: '#7a42f4',
       padding: 10,
       margin: 15,
       height: 40,
    },
    submitButtonText:{
       color: 'white',
       textAlign : 'center'
    }
 })
