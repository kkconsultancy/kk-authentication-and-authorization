import React , {Component} from 'react';
import {View, Text, TouchableOpacity,TextInput,StyleSheet} from 'react-native';

class Inputs extends Component {
    state = {
        name:'',
        username:'',
        email:'',
        confirmemail:'',
        phoneno:'',
        confirmphoneno:'',
        password:''
    }
    handlename = (text) => {
        this.setState({name:text})
    }
    handleusername = (text) => {
        this.setState({username:text})
    }
    handleemail = (text) => {
        this.setState({email:text})
    }
    handleconfirmemail = (text) => {
        this.setState({confirmemail:text})
    }
    handlephoneno = (number) =>  {
        this.setState({phoneno:number})
    }
    handleconfirmphoneno = (number) => {
        this.setState({confirmphoneno:number})
    }
    handlepassword = (text) => {
        this.setState({password:text})
    }
    render() {
        return(
            <View style = {styles.container}>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Name"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handlename}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Username"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleusername}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Email"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleemail}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Confirm Email"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleconfirmemail}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Phone Number"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handlephoneno}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Confirm Phone Number"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleconfirmphoneno}/>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Password"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handlepassword}/>
            </View>
        )
    }
}
export default Inputs

const styles = StyleSheet.create({
    container: {
       paddingTop: 23
    },
    input: {
       margin: 15,
       height: 40,
       borderColor: '#7a42f4',
       borderWidth: 1,
       fontWeight:'bold'
    },
    submitButton: {
       backgroundColor: '#7a42f4',
       padding: 10,
       margin: 15,
       height: 40,
    },
    submitButtonText:{
       color: 'white',
       textAlign : 'center'
    }
 })
