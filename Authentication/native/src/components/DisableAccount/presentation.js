import React,{Component} from 'react'
import { TouchableOpacity,TextInput, Text, View, StyleSheet } from 'react-native';

const Presentation = (props) => {
    return (
        <View>
             <View style = {styles.container}>
             <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "       Username"
                 placeholderTextColor = "#9e9e9e"
                 autoCapitalize = "none"
                 onChangeText = {this.handleEmail}/>

                 <TextInput style = {styles.input}
                 underlineColorAndroid = "transparent"
                 placeholder = "       Password"
                 placeholderTextColor = "#9e9e9e"
                 autoCapitalize = "none"
                 onChangeText = {this.handleEmail}/>

                
  
        <TouchableOpacity style = {styles.submitButton}>
                 <Text style = {styles.submitButtonText} > DISABLE ACCOUNT </Text>
              </TouchableOpacity>
                
            </View>
             
         </View>
    )
 }

 export default Presentation

 
 const styles = StyleSheet.create({
     textView : {
         textAlign:'center',
         fontSize : 18,
         fontWeight: 'bold'
     },container: {
        paddingTop: 23
     },
     input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1,
        fontWeight:'bold'
     },
     submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
     },
     submitButtonText:{
        color: 'white',
        textAlign : 'center'
     }
 })