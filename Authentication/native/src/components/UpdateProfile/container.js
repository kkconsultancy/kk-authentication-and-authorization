

import React,{Component} from 'react';
import {} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Presentation from './presentation.js'
 
class Container extends Component{


     
    
      state = {
        username:'',
        password:''
    }
    handleusername = (text) => {  
        this.setState({username:text});
    }
    handlepassword = (text) => {
        this.setState({password:text})
    }

    render(){
        return(
            <Presentation  {...this.state}  goToDashboard={this.goToDashboard} goToRegister={this.goToRegister}  goToResetPassword={this.goToResetPassword} 
            handlepassword={this.handlepassword} handleusername={this.handleusername}/>
        )
    }
}
export default Container