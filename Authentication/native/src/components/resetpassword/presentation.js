import React , {Component} from 'react';
import {View, TextInput,StyleSheet} from 'react-native';


class SendCode extends Component {
    state = {
        username:''
    }
    handleusername = (text) => {
        this.setState({username:text})
    }
    render() {
        return(
            <View style = {styles.container}>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Username"
                    autoCapitalize = "none"
                    placeholderTextColor = "#9a73ef"
                    onChangeText = {this.handleusername}/>
            </View>
        )
    }
}

export default SendCode


const styles = StyleSheet.create({
    container: {
       paddingTop: 23
    },
    input: {
       margin: 15,
       height: 40,
       borderColor: '#7a42f4',
       borderWidth: 1,
       fontWeight:'bold'
    },
    submitButton: {
       backgroundColor: '#7a42f4',
       padding: 10,
       margin: 15,
       height: 40,
    },
    submitButtonText:{
       color: 'white',
       textAlign : 'center'
    }
 })
