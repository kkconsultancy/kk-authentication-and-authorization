import React from 'react';
import {View,Alert,TouchableOpacity,Text,StyleSheet} from 'react-native';
import SendCode from './presentation.js';

const ResetPassword = () => {
   const showAlert = () =>{
      Alert.alert(
         'Verification Link has been sent to your Email..'
      )
   }
    return (
        <View>
           <SendCode />
           <TouchableOpacity onPress = {showAlert}  style = {styles.submitButton}>
                <Text style = {styles.submitButtonText} > SEND VERIFICATION LINK </Text>
            </TouchableOpacity>
        </View>
   )
}
export default ResetPassword


const styles = StyleSheet.create({
   container: {
      paddingTop: 23
   },
   input: {
      margin: 15,
      height: 40,
      borderColor: '#7a42f4',
      borderWidth: 1,
      fontWeight:'bold'
   },
   submitButton: {
      backgroundColor: '#7a42f4',
      padding: 10,
      margin: 15,
      height: 40,
   },
   submitButtonText:{
      color: 'white',
      textAlign : 'center'
   }
})




