const functions = require('firebase-functions');
const cors = require('cors')({origin : true});
const admin = require("firebase-admin");
admin.initializeApp({
    credential : admin.credential.cert(require('./json/admin.json'))
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        response.status(200).send("Hello from Firebase!");
    });
});

exports.getFirebaseConfig = functions.https.onRequest((request,response) => {
    const conditions = {hostname:request.hostname,type:request.method,parameters:request.body}
    console.log(conditions)

    cors(request,response,() => {

        if(conditions.type !== 'POST'){
            response.status(200).send('DUDE NO TIME FOR THIS PLEASE!!');
            console.log(conditions);
            return;
        }
        if(conditions.parameters.password !== "real password"){
            response.status(200).send('YOU ARE NOT THE GIVEUP TYPE :)');
            console.log(conditions);
            return;
        }
        const client = require('./json/client.json');
        response.status(200).send(client);
    });
});

exports.createUser = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };

    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().createUser(conditions.parameters)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
        
    });
});

exports.authenticateUser = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions);
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            return;
        }
        const data = {
            uid : conditions.parameters.uid,
            additionClaims : {
                admin : conditions.parameters.accountType
            }
        }
        admin.auth().createCustomToken(data.uid)
        .then(res => {
            response.status(200).send(res)
        }).catch(err => {
            response.status(200).send(err)
        });
        
    });
});

exports.deauthenticateUser = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }

        response.status(200).send("Feature is yet to be ready");
    });
});

exports.updateUser = functions.https.onRequest((request,response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().updateUser(conditions.parameters.uid,conditions.parameters.data)
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
    });
});

exports.deleteUser = functions.https.onRequest((request,response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        const data = {
            uid : conditions.parameters.uid
        }
        admin.auth().deleteUser(data.uid)
        .then(res => {
            response.status(200).send(res);
        })
        .catch(err => {
            response.status(200).send(err);
        });
    });
});

exports.disableUser = functions.https.onRequest((request,response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().updateUser(conditions.parameters.uid, {emailVerified : false,disabled:true})
        .then(res => {
            response.status(200).send(res);
        }).catch(err => {
            response.status(200).send(err);
        })
        
    });
});

exports.fetchUserById = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    console.log(conditions)
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().getUser(conditions.parameters.uid)
            .then(res => {
                response.status(200).send(res);
            })
            .catch(err => {
                response.status(200).send(err);
            });
    });
});
exports.fetchUserByEmail = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    console.log(conditions)
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().getUserByEmail(conditions.parameters.email)
            .then(res => {
                response.status(200).send(res);
            })
            .catch(err => {
                response.status(200).send(err);
            });
    });
});
exports.fetchUserByPhoneNumber = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body };
    console.log(conditions)
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            response.status(200).send('NEVER MESS WITH HTML HACKERS!!');
            console.log(conditions);
            return;
        }
        admin.auth().getUserByPhoneNumber(conditions.parameters.phoneNumber)
            .then(res => {
                response.status(200).send(res);
            })
            .catch(err => {
                response.status(200).send(err);
            });
    });
});

