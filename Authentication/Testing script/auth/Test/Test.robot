*** Settings ***
Library  Selenium2Library

Library  ../Resources/Csv.py
Resource  ../Resources/po/Common.robot
Resource  ../Resources/po/kk001-001/Resgesteration-types.robot
Resource  ../Resources/common.robot

*** Keywords ***
Get CSV Data
    [Arguments]  ${FilePath}
    ${Data}  read csv file  ${FilePath}
    [Return]  ${Data}
*** Test Cases ***
Auth0
    ${ValidSigninScenrios}  Get CSV Data  Data/Data1.csv
    :FOR  ${row}  IN  @{ValidSigninScenrios}
    \  Open
    \  Go Page To SignUp
    \  KK001-001  ${row}
    \  KK001-002  ${row}
    \  KK001-003  ${row}
    \  Close
Auth1
    ${ValidSigninScenrios}  Get CSV Data  Data/Data.csv
    :FOR  ${row}  IN  @{ValidSigninScenrios}
    \  Open
    \  KK001-004  ${row}
    \  KK001-006  ${row}
    \  #KK001-007,008
    \  KK001-009


