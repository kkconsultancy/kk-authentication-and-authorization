*** Settings ***
Library  Selenium2Library
*** Keywords ***
changing password
    [Arguments]  ${hr}
    #clicking the change password button
    click element  //*[@id="root"]/div/div[2]/h4/a
    #Giving text into the box
    input text  //*[@id="username"]  ${hr[1]}
    #checking the error
    page should contain  ${hr[3]}
    #clicking the reset button
    click element  //*[@id="root"]/div/form/div[3]/div[1]/button
    page should contain  ${hr[4]}
    #clicking the back buton
    click element  //*[@id="root"]/div/form/div[3]/div[2]/a/button

