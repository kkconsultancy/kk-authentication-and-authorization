*** Settings ***
Library  Selenium2Library
Resource  ../Resources/po/kk001-001/Resgesteration-types.robot
Resource  ../Resources/po/KK001-002/Login.robot
Resource  ../Resources/po/KK001-003/Resetpassword.robot
Resource  ../Resources/po/KK001-006/changepassword.robot
Resource  ../Resources/po/KK001-007,008/disable.robot
Resource  ../Resources/po/KK001-009/Signout.robot
*** Keywords ***
KK001-001
    [Arguments]  ${KK001-001}
    regestration2  ${KK001-001}
KK001-002
    [Arguments]  ${KK001-002}
    Login invalid  ${KK001-002}
    Login with ungester account  ${KK001-002}
KK001-003
    [Arguments]  ${KK001-003}
    Forget Password  ${KK001-003}
KK001-004
    [Arguments]  ${KK001-004}
    Login valid  ${KK001-004}
KK001-006
    [Arguments]  ${KK001-006}
    changing password   ${KK001-006}
KK001-007,008
    [Arguments]  ${KK001-006}
    diableAccount
    delete account
KK001-009
    sign out