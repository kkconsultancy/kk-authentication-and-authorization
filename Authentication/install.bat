@echo off
FOR /F "tokens=* USEBACKQ" %%F IN (`node -v`) DO (
SET VAR=%%F
)
IF /I "%VAR%" NEQ "v10.15.3" (
	echo "Node verision required -- v10.15.3"
	PAUSE
	EXIT
)

FOR /F "tokens=* USEBACKQ" %%F IN (`npm -v`) DO (
SET VAR=%%F
)
IF /I "%VAR%" NEQ "6.4.1" (
	echo "Required npm version 6.4.1"
	npm install -g npm@6.4.1
)

FOR /F "tokens=* USEBACKQ" %%F IN (`firebase --version`) DO (
SET VAR=%%F
)
IF /I "%VAR%" NEQ "6.8.0" (
	echo "Required firebase-tools version 6.8.0"
	npm install -g firebase-tools@6.8.0
)

npm install
cd functions
npm install
cd ..
PAUSE
