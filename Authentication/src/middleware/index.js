import axios from 'axios';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage';

class Firebase{
    state = {
        firebase : firebase
    }
    firebaseUrl = "http://localhost:5001/kk-authenticate-and-authorize/us-central1";
    // firebaseUrl = "https://us-central1-kk-authenticate-and-authorize.cloudfunctions.net";

    signup = (data,callback) => {
        axios.post(this.firebaseUrl + "/createUser", data)
        .then(res => {
            console.log(res.data)
            if(res.data.code){
                callback({
                    error : true,
                    code : res.data.code,
                    message : res.data.message
                });
            }else{
                callback({
                    error: false,
                    next : 'login'
                });
            }
        }).catch(err => {
            console.log(err)
            callback({
                error: true,
                code: err.code,
                message: err.message
            });
        })
    }

    signin = (data,callback) => {
        // console.log(data)
        this.state.auth.signInWithEmailAndPassword(data.email, data.password)
        .then(res => {
            if(!res.user.emailVerified || res.user.disabled)
            this.state.auth.currentUser.sendEmailVerification()
            .then(() => {
                const action = {
                    error : false,
                    next : 'emailVerification'
                }
                callback(action)
            }).catch(err => {
                console.log(err)
                callback({
                    error: true,
                    code: err.code,
                    message: err.message
                });
            });
            else{ 
                localStorage.setItem("kkemail",data.email);
                localStorage.setItem("kkuid", res.user.uid);
                localStorage.setItem("kk"+localStorage.getItem("kkuid")+"password", data.password);
                res.user.next = "dashboard";
                callback(res.user);
            }
        })
        .catch(err => {
            // console.log(err)
            callback({
                error: true,
                code: err.code,
                message: err.message
            })
        });
        // console.log(data)
    }

    signout = (callback) => {
        this.state.auth.signOut()
        .then(res => {
            console.log(res);
            const action = {
                error: false,
                next: '/login'
            }
            localStorage.removeItem("kk"+localStorage.getItem("kkuid")+"password");
            localStorage.removeItem('kkuid');
            callback(action);
        }).catch(err => {
            console.log(err);
            const action = {
                error: false,
                next: '/login'
            }
            callback(action);
        })

        // axios.post(this.firebaseUrl + "/deauthenticateUser", data)
        // .then(res => {
        //     console.log(res)
        // }).catch(err => {
        //     console.log(err)
        // })
    }

    resetPassword = (data,callback) => {
        
        firebase.auth().sendPasswordResetEmail(data.email)
        .then(res =>{
            const action = {
                error: false,
                next: 'emailReset'
            }
            callback(action)
        }).catch(err => {
            console.log(err)
            callback({
                error: true,
                code: err.code,
                message: err.message
            });
        });

        // axios.post(this.firebaseUrl + "/resetPassword", data)
        // .then(res => {
        //     console.log(res)
        // }).catch(err => {
        //     console.log(err)
        // })
    }

    updateProfile = (parameters,callback) => {
        const data = {
            password : parameters.password,
            displayName : parameters.displayName,
            phoneNumber : parameters.phoneNumber,
            email : parameters.email,
            emailVerified : parameters.emailVerified
        }
        axios.post(this.firebaseUrl + "/updateUser", {uid : parameters.uid,data : data})
        .then(res => {
            console.log(res)
        }).catch(err => {
            console.log(err)
        })
    }

    deleteAccount = (data,callback) => {
        this.state.auth.signOut();
        axios.post(this.firebaseUrl + "/deleteUser", data)
        .then(res => {
            const action = {
                next : 'login'
            }
            callback(action);
        }).catch(err => {
            console.log(err)
            err.error = true
            callback(err)
        })
    }

    disableAccount = (data,callback) => {
        axios.post(this.firebaseUrl + "/disableUser", data)
        .then(res => {
            this.state.auth.signOut();
            res.data.next = 'login'
            callback(res.data);
        }).catch(err => {
            err.error = true
            callback(err)
        })
    }

    constructor(callback){
        axios.post(this.firebaseUrl+"/getFirebaseConfig",{password : "real password"})
        .then(res => {
            this.state.config = res.data;
            this.state.firebase.initializeApp(this.state.config);
            this.state.auth = this.state.firebase.auth();
            this.state.firestore = this.state.firebase.firestore();
            this.state.database = this.state.firebase.database();
            this.state.storage = this.state.firebase.storage();
            this.state.callback = callback;
            this.state.callback('firebaseReady', true);
            console.log([
                localStorage.getItem("kkemail"),
                localStorage.getItem("kk"+localStorage.getItem("kkuid")+"password")])
        }).catch(err => {
            console.log(err)
            callback('error', "PLEASE MAKE SURE YOUR CONNECTION IS STABLE");
        });


    }

    getUserById = (data, callback) => {
        axios.post(this.firebaseUrl + "/fetchUserById", data)
            .then(res => {
                if (res.data.code) {
                    callback({
                        error: true,
                        code: res.data.code,
                        message: res.data.message
                    });
                } else callback(res);
            }).catch(err => {
                console.log(err);
                callback({
                    error : true,
                    code : err.code,
                    message : err.message
                })
            });
    }
    getUserByEmail = (data, callback) => {
        axios.post(this.firebaseUrl + "/fetchUserByEmail", data)
            .then(res => {
                if (res.data.code) {
                    callback({
                        error: true,
                        code: res.data.code,
                        message: res.data.message
                    });
                } else callback(res);
            }).catch(err => {
                console.log(err)
                callback({
                    error: true,
                    code: err.code,
                    message: err.message
                })
            });
    }
    getUserByPhoneNumber = (data, callback) => {
        console.log(data)
        axios.post(this.firebaseUrl + "/fetchUserByPhoneNumber", data)
            .then(res => {
                console.log(res);
                if (res.data.code) {
                    callback({
                        error: true,
                        code: res.data.code,
                        message: res.data.message
                    });
                }else callback(res);
            }).catch(err => {
                console.log(err)
                callback({
                    error: true,
                    code: err.code,
                    message: err.message
                })
            });
    }
}

export default Firebase;