import React from 'react';

const Presentation = props =>(
    <div className="container card">
        <div className="row justify-content-md-center">
            <h1>Verification Link has been sent to your registered mail</h1>
        </div>
    </div>
)

export default Presentation;