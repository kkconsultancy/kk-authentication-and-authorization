import React from 'react'
import Presentation from './presentation';

class Container extends React.Component{
    constructor(props){
        super(props)
        this.props = props
    }

    render(){
        return(
            <Presentation/>
        )
    }
}

export default Container