import React from 'react'

const Presentation = props => (
    <div className="container middle card">
        <div className="row">
            <h1>Reset Link has been sent to your registered mail</h1>
        </div>
    </div>
)

export default Presentation