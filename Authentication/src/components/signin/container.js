import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';
import Presentation from './presentation';

class Container extends Component{
    state = {
        username : '',
        password : '',
        email:'',
        submitDisabled: false,
        resetDisabled: false,
        next: '',
        error:''
    }

    constructor(props){
        super(props)
        this.props = props;
    }

    stateSet = (id,message) => {
        this.setState({
            [id] : message
        })
    }

    componentWillMount(){
        if(localStorage.getItem('kkuid'))
        this.props.signin({
            email : localStorage.getItem("kkemail"),
            password : localStorage.getItem("kk"+localStorage.getItem("kkuid")+"password")
        },this.callback);
    }

    callback = (action) => {
        this.setState({
            submitDisabled : false,
            resetDisabled : false
        });
        if(action.error)
            this.setState({ error: action.code.split('/')[1] + " : " + action.message})
        else{
            this.setState({next : <Redirect to={"/"+action.next}/>})
        }
    }

    makeSignin = (response) => {
        if(response.code){
            this.callback(response);
            return;
        }
        const data = {
            email : response.data.email,
            password : this.state.password
        }
        if(!data.email){
            this.stateSet("submitDisabled", false);
            this.stateSet("resetDisabled", false);
            return;
        }else{
            this.stateSet("error",'');
        }
        this.props.signin(data, this.callback);
    }

    formUpdate = (e) => {
        this.stateSet(e.target.id, e.target.value)
    }

    formSubmit = (e) => {
        e.preventDefault();
        const data = {
            username : this.state.username,
            password : this.state.password
        }
        this.stateSet("submitDisabled",true);
        this.stateSet("resetDisabled",true);
        const mailExp = /^.*@.*\..*/;
        const phoneExp = /^\+[0-9]+/;

        if (data.username.match(mailExp)) {
            this.makeSignin({data:{email : this.state.username}});
        } else if (data.username.match(phoneExp)) {
            this.props.fetchDetails.byPhoneNumber({phoneNumber : data.username},this.makeSignin);
        }else{
            this.props.fetchDetails.byId({uid : data.username}, this.makeSignin);
        }        
    }

    render(){
        if(this.state.error) return(
            <Presentation
                {...this.state}
                formSubmit={this.formSubmit}
                formUpdate={this.formUpdate}
            />
        )
        else if(localStorage.getItem('kkuid')) return(
            <div className="container ">
                <div className="d-flex justify-content-center">
                    <strong className="row badge-info">--- LOADING ---</strong>
                </div>
                {this.state.next}
            </div>
        )
        else return (
            <Presentation
                {...this.state}
                formSubmit={this.formSubmit}
                formUpdate={this.formUpdate}
            />
        )
    }

}

export default Container;