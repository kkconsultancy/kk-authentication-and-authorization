import React from 'react'
import {Redirect} from 'react-router-dom'
import Presentation from './presentation'

class Container extends React.Component{
    state = {
        username : '',
        mobile : '',
        email : '',
        password : '',
        error : ''
    }

    constructor(props){
        super(props)
        this.props = props;
    }

    stateSet = (id,message) => {
        this.setState({
            [id] : message
        })
    }

    callback = (action) => {
        this.setState({
            submitDisabled : false,
            resetDisabled : false
        })
        if(action.code)
            this.setState({error : action.code.split('/')[1] +" : "+ action.message})
        else{
            this.setState({
                error : '',
                next : <Redirect to={"/"+ action.next}/>
            })
        }
    }

    usernameCheck = (action) => {
        if(!action.error){
            this.stateSet("usernameError","Username already exists can't be accepted!")
        }else{
            this.stateSet("usernameError", "")
        }
    }

    formUpdate = (e) => {
        this.stateSet(e.target.id, e.target.value)

        if(e.target.id === "username"){
            this.props.getUserById({ uid: e.target.value }, this.usernameCheck);
        }

        if (e.target.id === "email") {
            const reg = /^.+@.+\..+/
            if (e.target.value.match(reg) === null) {
                this.stateSet("emailError", "Incorrect Email");
            }
            else if (e.target.value.length === 0)  this.stateSet("emailError", "");
            else{
                this.stateSet("emailError", "");
            }
            return;
        }

        if(e.target.id === "mobile"){
            if(!e.target.value.match(/^\+[0-9]{1,3}[0-9]{10}/))
            // if(!(e.target.value.length===10))
                this.stateSet("phoneError", "Incorrect mobile number, Please specify the country code with the mobile number.");
            else{
                this.stateSet("phoneError", "");
            }
            return;
        }

        if (e.target.id === "password"){
            if (e.target.value !== this.state.cpassword) {
                this.stateSet("cpasswordError","Confirmation failed");
            } else {
                this.stateSet("cpasswordError", "");
            }
            return;
        }
        if (e.target.id === "cpassword") {
            if (e.target.value !== this.state.password) {
                this.stateSet("cpasswordError", "Confirmation failed");
            } else{
                this.stateSet("cpasswordError", "");
            }
            return;
        }
    }

    formSubmit = (e) => {
        e.preventDefault();

        const data = {
            displayName : this.state.name,
            uid : this.state.username,
            password : this.state.password,
            email : this.state.email,
            phoneNumber : this.state.mobile
        }

        if(this.state.emailError || this.state.phoneError || this.state.usernameError)
            return;

        this.stateSet("submitDisabled",true);
        this.stateSet("resetDisabled",true);

        this.props.signup(data,this.callback);
    }

    render(){
        return (
            <Presentation
                {...this.state}
                formSubmit = {this.formSubmit}
                formUpdate = {this.formUpdate}
            />
        )
    }

}

export default Container;