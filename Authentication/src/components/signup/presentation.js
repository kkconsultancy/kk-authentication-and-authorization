import React from 'react'
import {Link} from 'react-router-dom'

const Presentation = props => (
    <div >                
        {props.next}
        <div className="row badge badge-danger">{props.error}</div>
        <form onSubmit={props.formSubmit} className="container">
            <div className="row">
                <label>Name* : (First Name + Last Name)</label>
                <input type="text" className="form-control" onChange={props.formUpdate} id="name" />
            </div>
            <div className="row">
                <label>Username* :</label>
                <input type="text" className="form-control" onChange={props.formUpdate} id="username"/>
            </div>
            <div className="row badge badge-danger">{props.usernameError}</div>
            <div className="row">
                <label>Email* :</label>
                <input type="email" className="form-control" onChange={props.formUpdate} id="email"/>
            </div>
            <div className="row badge badge-danger">{props.emailError}</div>
            <div className="row">
                <label>Phone no.* :</label>
                <input type="text" className="form-control" onChange={props.formUpdate} id="mobile"/>
            </div>
            <div className="row badge badge-danger">{props.phoneError}</div>
            <div className="row">
                <label>Password* :</label>
                <input type="password" className="form-control" onChange={props.formUpdate} id="password" />
            </div>
            <div className="row">
                <label>Confirm Password* :</label>
                <input type="password" className="form-control" onChange={props.formUpdate} id="cpassword" />
            </div>
            <div className="row badge badge-danger">{props.cpasswordError}</div>
            <br/>
            <div className="row">
                <div className="col"><button disabled={props.submitDisabled} onClick={props.formSubmit} className="btn btn-success btn-lg btn-block">Submit</button></div>
                <div className="col"><button type="reset" disabled={props.resetDisabled} className="btn btn-warning btn-lg btn-block">Reset</button></div>
            </div>
            <div className="row" align="center">
                <div className="col">
                    <h4>Have an account?
                        <Link className="text-decoration-none" to="/login"> Signin</Link></h4>
                </div>
            </div>
        </form>
    </div>
)

export default Presentation;