import React from 'react'
import Presentation from './presentation'
import {Redirect} from 'react-router-dom'

class Container extends React.Component{
    state = {
        error:''
    }
    componentDidMount(){
        if(!this.props.user){
            this.setState({
                next : <Redirect to="/dashboard"/>
            })
        }
    }
    constructor(props){
        super()
        this.props = props
        if(this.props.user){
            const data= {
                uid : this.props.user.uid
            }
            console.log(data)
            this.props.disableAccount(data,this.callback)
        }
    }

    callback = (action) => {
        console.log(action)
        if(action.code)
            this.setState({error : action.message})
        else{
            this.setState({next : <Redirect to={"/"+action.next}/>})
        }
    }

    render(){
        return(
            <Presentation
                {...this.state}
            />
        )
    }
}

export default Container