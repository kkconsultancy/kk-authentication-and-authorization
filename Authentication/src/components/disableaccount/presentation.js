import React from 'react'

const Presentation = props => (
    <div className="container">
        {props.next}
        Deactivated!
        <div className="error">{props.error}</div>
    </div>
)

export default Presentation