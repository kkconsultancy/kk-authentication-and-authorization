import React from 'react'
import {Link} from 'react-router-dom'

const Presentation = props => (
    <div className="container bg-light rounded">
        {props.next}
        <div className="row badge badge-danger">{props.error}</div>
        <form onSubmit={props.formSubmit} className="container">
            <div className="row">
                <label>Username* :</label>
                <input type="text" onChange={props.formUpdate} className="form-control" id="username"/>
            </div>
            <div className="row badge badge-danger">{props.usernameError}</div>
            <br/>
            <div className="row">
                <div className="col">
                    <button onClick={props.formSubmit} disabled={props.submitDisabled}
                        className="btn btn-primary btn-lg btn-block">
                        Reset Password
                    </button>
                </div>
                <div className="col">
                    <Link to="/login">
                        <button type="reset"
                            disabled={props.resetDisabled}
                            className="btn btn-secondary btn-lg btn-block">
                            Back
                        </button>
                    </Link>
                </div>
            </div>  
        </form>
    </div>
)

export default Presentation