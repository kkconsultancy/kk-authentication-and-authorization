import React from 'react'
import {Link} from 'react-router-dom'

const Presentation = props => (
    <div className="container">
        <div className="row">
                <h4>HOME</h4>
        </div>
        <div className="row">
            <h4><Link className="text-decoration-none" to="/signin"> Signin</Link></h4>
        </div>
    </div>
)

export default Presentation