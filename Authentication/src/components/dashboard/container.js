import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';
import Presentation from './presentation';

class Container extends Component{
    state = {}
    action = {}
    props = {}
    constructor(props){
        super(props)
        this.props = props
    }



    render(){
        if(!this.props.user) return(
            <Redirect to='/login'/>
        )
        else if(!this.props.user.emailVerified) return(
            <Redirect to='/login'/>
        )
        else return (
            <Presentation user = {this.props.user}/>
        )
    }
}

export default Container;