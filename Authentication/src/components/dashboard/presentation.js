import React from 'react'
import {Link} from 'react-router-dom';

const Presentation = props => (
    <div className="container">
        <div className="row">
            <h1>Hello {props.user.displayName}</h1>
        </div>
        <div className="row">
            <h4><Link to="/changePassword">Change Password</Link><br/></h4>
        </div>
        <div className="row">
            <h4><Link to="/disableAccount">Disable/De-activate</Link><br/></h4>
        </div>
        <div className="row">
            <h4><Link to="/deleteAccount">Delete</Link></h4>
        </div>
        <div className="row">
            <h4><Link to="/signOut">signout</Link></h4>
        </div>
    </div>
)

export default Presentation