import React from 'react'

const Presentation = props => (
    <div className="SignOut form-group">
        {props.next}
        <div className="error">{props.error}</div>
    </div>
)

export default Presentation