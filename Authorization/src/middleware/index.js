import axios from 'axios';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage';
import GetClosedData from './getClosedData';

class Firebase{
    state = {
        firebase : firebase,
        auth : {
            onAuthStateChanged : null
        }
    }
    firebaseUrl = "http://localhost:5001/redants4site/us-central1";
    firebaseUrl = "https://us-central1-redants4site.cloudfunctions.net";

    signin = (data,callback) => {
        // console.log(data)
        this.state.auth.signInWithEmailAndPassword(data.email, data.password)
        .then(res => {
            // if(!res.user.emailVerified || res.user.disabled)
            // this.state.auth.currentUser.sendEmailVerification()
            // .then(() => {
            //     const action = {
            //         error : false,
            //         next : 'emailVerification'
            //     }
            //     callback(action)
            // }).catch(err => {
            //     console.log(err)
            //     callback(err);
            // });
            // else{
                callback(res.user);
                this.state.firestore.collection('PROFILES').doc(res.user.uid).get()
                .then(result => {
                    localStorage.setItem(res.user.uid, JSON.stringify(result.data()))
                }).catch(err => {
                    console.log(err)
                })
            // }
        })
        .catch(err => {
            // console.log(err)
            callback({
                error: true,
                code: err.code,
                message: err.message
            })
        });
        // console.log(data)
    }
    signOut = () => {
        this.state.auth.signOut().then(()=>{
            this.state.callback({ 
                'firebaseReady': true, userReady: false, isChecking: false 
            })
        })
    }
    initializeApp(firebaseConfig){
        this.state.firebase.initializeApp(firebaseConfig);
        this.state.auth = this.state.firebase.auth();
        this.state.firestore = this.state.firebase.firestore();
        this.state.database = this.state.firebase.database();
        this.state.storage = this.state.firebase.storage();
        this.state.auth.onAuthStateChanged((user) => {
            if (user) {
                // User is signed in.
                // console.log(user);
                if (!localStorage.getItem(user.uid))
                    this.state.firestore.collection('PROFILES').doc(user.uid).get()
                    .then(res => {
                        localStorage.setItem(user.uid, JSON.stringify(res.data()))
                    }).catch(err => {
                        console.log(err)
                    })
                this.state.callback({ 'firebaseReady': true, userReady: true, isChecking: false })
                new GetClosedData(this.state)
            } else {
                // No user is signed in.
                this.state.callback({ 'firebaseReady': true, userReady: false, isChecking: false })
            }
        });
        this.state.callback({ 'firebaseReady': true, isChecking: true });
    }

    constructor(callback){
        // console.log(localStorage.getItem('firebaseConfig'))
        this.state.callback = callback
        if(localStorage.getItem('firebaseConfig')){
            this.initializeApp(JSON.parse(localStorage.getItem('firebaseConfig')))
        }else
        axios.post(this.firebaseUrl + "/getFirebaseConfig", {password: "please dont ask me"})
        .then(res => {
            localStorage.setItem("firebaseConfig",JSON.stringify(res.data))
            this.initializeApp(JSON.parse(localStorage.getItem('firebaseConfig')))
        }).catch(err => {
            callback(err);
        });
    }
    

}

export default Firebase;