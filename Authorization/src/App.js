import React,{Component} from 'react';
import {BrowserRouter,Route,Redirect,Switch} from 'react-router-dom';

import Firebase from './middleware';

import Attendance from './components/attendance/';
import Dashboard from './components/dashboard/';
import Login from './components/signin/';
import Logout from './components/logout'
import NavBar from './components/navigation'
import Footer from './components/footer'

class App extends Component {
  state = {
    message:"---LOADING---"
  }
  firebase = null
  componentDidMount(){
    this.firebase = new Firebase((res) => {
      // console.log(res)
      for(let i in res){
        this.setState({
          [i] : res[i]
        })
      }
    });
  }
  render(){
    return(
      <BrowserRouter>
        <NavBar middleware={this.firebase} state={this.state}/>
        {(()=>{
          if(!this.state.firebaseReady)
            return(
              <div>
                {this.state.message}
              </div>
            )
          else if (this.state.isChecking)
            return (
              <div>
                {this.state.message}
              </div>
            )
          else if(this.state.userReady){
            return (
              <Switch className="h-75 d-inline-block">.
                <Route path="/dashboard" component={() => <Dashboard middleware={this.firebase} />} />
                <Route path="/manageattendance" component={() => <Attendance middleware={this.firebase} />} />
                <Route path="/signout" component={() => <Logout middleware={this.firebase} />} />
                <Redirect from="**" to="/dashboard"/>
              </Switch>
            )
          }
          else
          return (
              <div>
                <Switch>
                  <Route path="/login" component={() => <Login firebase={this.firebase}/>}/>
                  <Redirect from="**" to="/login" />
                </Switch>
              </div>
          );    
        })()}
        <Footer/>
      </BrowserRouter>
    )
  }
}

export default App;
