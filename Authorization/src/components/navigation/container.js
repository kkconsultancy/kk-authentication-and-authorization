import React,{Component} from 'react';
import Presentation from './presentation';
import {Link} from 'react-router-dom';

class Container extends Component{
    navsInsert = (services) =>{
        const navItems = services.map(s => {
            return( 
                <li key={s.id} className="nav-item active">
                    <Link className="nav-link" to={"/"+s.route}>{s.name}</Link>
                </li>
            )
        })
        return navItems;
    }
    componentWillMount(){
        const data = [
            {
                id: 1,
                route: "dashboard",
                name : "HOME"
            },{
                id: 2,
                route:"manageattendance",
                name:"ATTENDANCE"
            },{
                id: 3,
                route:"signout",
                name:"LOGOUT",
            }
        ]
        this.navItems = this.navsInsert(data)
    }
    render(){
        return(
            <Presentation
                navItems={this.navItems}
                state={this.props.state}
                middleware={this.props.middleware}
            />
        )
    }
}

export default Container;