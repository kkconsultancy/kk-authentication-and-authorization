import React from 'react'
import {Link} from 'react-router-dom'
 
const Presentation = props => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-danger static-top sticky-top">
        <div className="container">
            <a className="navbar-brand" href="/">
                <img src="https://www.siliconindia.com/images/education/19072.jpeg" width="75" alt=""/>
                <span className="ml-1">SITE</span>              
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
                {
                    (() => {
                        if(props.state.firebaseReady)
                            if(props.state.userReady){
                                return props.navItems
                            }
                        if(!props.state.isChecking)
                        return(
                            <li className="nav-item active">
                                <Link className="nav-link" to="/login">LOGIN</Link>
                            </li>
                        )
                    })()
                }
                
            </ul>
            </div>
        </div>
    </nav>
)

export default Presentation