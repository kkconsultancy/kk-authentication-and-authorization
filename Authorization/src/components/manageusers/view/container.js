import React,{Component} from 'react'
import Presentation from './presentation';

class Container extends Component{
    state={
        name:"",
        uid:"",
        uname:"",
        email:"",
        phone:"",
        pswd:"",
        cPswd:"",
        error:"",
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }

    handleCancel = (e) => {
        this.setState({error:""})
    }

    render(){
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                handleCancel={this.handleCancel}
            />
        )        
    }
}

export default Container