import React from 'react'

const Presentation = props =>(
    <div>
        <h1>View User!</h1>
        <form>
            <div>{props.error}</div>
            <div>
                <label>UserID</label>
                <input type="text" placeholder="email/username/phone" id="uid"></input>
            </div>
            <div>       
                <button type="submit" onClick={props.handleSubmit}>Submit</button>
                <button type="reset" id="reset" >Cancel</button>
            </div>
        </form>
        <br/>
        <div style={{display:'none'}}>
            <div>  
                <label>Name</label>
                <input type="text" placeholder="Full Name" id="name" disabled={true}/>
            </div>
            <div> 
                <label>Username</label>
                <input type="text" placeholder="@user_0001" id="uname"disabled={true}/>
            </div>
            <div>    
                <label>Email</label>
                <input type="email" placeholder="someone@url" id="email"disabled={true}/>
            </div>
            <div>    
                <label>Phone</label>
                <input type="text" placeholder="+919876543210" id="phone"disabled={true}/>
            </div>
            <div>    
                <label>Password</label>
                <input type="password" placeholder="******" id="pswd"disabled={true}/>
            </div>
            <div>    
                <label>Confirm Password</label>
                <input type="password" placeholder="******" id="cPswd"disabled={true}/>
            </div>
        </div>
    </div>
)

export default Presentation