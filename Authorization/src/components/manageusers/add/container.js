import React,{Component} from 'react'
import Presentation from './presentation';

class Container extends Component{
    state={
        name:"",
        uname:"",
        email:"",
        phone:"",
        pswd:"",
        cPswd:"",
        error:"",
    }
    handleChange = (e) => {
        this.setState({[e.target.id] : e.target.value})
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }

    render(){
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
            />
        )        
    }
}

export default Container