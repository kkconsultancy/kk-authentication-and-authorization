import React from 'react'

const Presentation = props =>(
    <div>
        <h1>Add User!</h1>
        <form>
            <div>{props.error}</div>
            <div>  
                <label>Name</label>
                <input type="text" onChange={props.handleChange} placeholder="Full Name" id="name" />
            </div>
            <div> 
                <label>Username</label>
                <input type="text" onChange={props.handleChange} placeholder="@user_0001" id="uname"/>
            </div>
            <div>    
                <label>Email</label>
                <input type="email" onChange={props.handleChange} placeholder="someone@url" id="email"/>
            </div>
            <div>    
                <label>Phone</label>
                <input type="text" onChange={props.handleChange} placeholder="+919876543210" id="phone"/>
            </div>
            <div>    
                <label>Password</label>
                <input type="password" onChange={props.handleChange} placeholder="******" id="pswd"/>
            </div>
            <div>    
                <label>Confirm Password</label>
                <input type="password" onChange={props.handleChange} placeholder="******" id="cPswd"/>
            </div>
            <div>       
                <button type="submit" onClick={props.handleSubmit}>Submit</button>
                <button type="reset" id="reset" >Cancel</button>
            </div>
        </form>
    </div>
)

export default Presentation