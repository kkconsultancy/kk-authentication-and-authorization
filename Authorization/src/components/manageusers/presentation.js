import React from 'react'
import {Route,Switch,Link} from 'react-router-dom';

import Add from './add'
import View from './view'
import Edit from './edit'
import Delete from './delete'

const Presentation = props =>(
    <div >
        <div>
            <h1>Manage Users</h1>
            <menu>
                <Link to="/manageUsers/add">Add</Link><br/>
                <Link to="/manageUsers/view">View</Link><br/>
                <Link to="/manageUsers/edit">Edit</Link><br/>
                <Link to="/manageUsers/delete">Delete</Link><br/>
            </menu>
        </div>
        <br/>
        <div>
            <Switch>
                <Route path="/manageUsers/add" component={()=><Add/>}/>
                <Route path="/manageUsers/view" component={()=><View/>}/>
                <Route path="/manageUsers/edit" component={()=><Edit/>}/>
                <Route path="/manageUsers/delete" component={()=><Delete/>}/>
            </Switch>
        </div>
    </div>
)

export default Presentation