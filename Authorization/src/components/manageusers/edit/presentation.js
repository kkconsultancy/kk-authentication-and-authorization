import React from 'react'

const Presentation = props =>(
    <div>
        <h1>Edit User!</h1>
        <form>
            <div>{props.error}</div>
            <div>
                <label>UserID</label>
                <input type="text" placeholder="" id="uid"></input>
            </div>
            <div>       
                <button type="submit" id="submit" onClick={props.handleSubmit}>Submit</button>
                <button type="reset" id="reset" >Cancel</button>
            </div>
        </form>
        <br/>
        <form style={{display:props.editDisplay}}>
            <div>{props.error}</div>
            <div>  
                <label>Name</label>
                <input type="text" onChange={props.handleChange} placeholder="Full Name" id="name" value={props.name}/>
            </div>
            <div> 
                <label>Username</label>
                <input type="text" onChange={props.handleChange} placeholder="@user_0001" id="uname" value={props.uname}/>
            </div>
            <div>    
                <label>Email</label>
                <input type="email" onChange={props.handleChange} placeholder="someone@url" id="email" value={props.email}/>
            </div>
            <div>    
                <label>Phone</label>
                <input type="text" onChange={props.handleChange} placeholder="+919876543210" id="phone" value={props.phone}/>
            </div>
            <div>    
                <label>Password</label>
                <input type="password" onChange={props.handleChange} placeholder="******" id="pswd" value={props.pswd}/>
            </div>
            <div>       
                <button type="submit" id="Save" onClick={props.handleSubmit}>Save</button>
                <button type="reset" id="reset" >Cancel</button>
            </div>
        </form>
    </div>
)

export default Presentation