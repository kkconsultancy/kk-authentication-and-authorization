import React,{Component} from 'react'
import Presentation from './presentation';

class Container extends Component{
    state={
        name:"",
        uname:"",
        email:"",
        phone:"",
        pswd:"",
        cPswd:"",
        error:"",
        editDisplay:"none"
    } 
    handleChange = (e) => {
        this.setState({[e.target.id] : e.target.value})
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            editDisplay:""
        })
        console.log(this.state)
    }

    handleCancel = (e) => {

    }

    render(){
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
            />
        )        
    }
}

export default Container