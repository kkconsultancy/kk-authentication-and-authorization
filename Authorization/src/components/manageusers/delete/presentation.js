import React from 'react'

const Presentation = props =>(
    <div>
        <h1>Delete User!</h1>
        <form>
            <div>{props.error}</div>
            <div>
                <label>UserID</label>
                <input type="text" placeholder="" id="uid"></input>
            </div>
            <div>       
                <button type="submit" onClick={props.handleSubmit}>Submit</button>
                <button type="reset" id="reset" >Cancel</button>
            </div>
        </form>
    </div>
)

export default Presentation