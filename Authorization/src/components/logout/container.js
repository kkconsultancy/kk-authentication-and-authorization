import React,{Component} from 'react'
import Presentation from './presentation';

class Container extends Component{
    componentDidMount(){
        this.props.middleware.signOut();
    }
    render(){
        return(
            <Presentation/>
        )
    }
}

export default Container