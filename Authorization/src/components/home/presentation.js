import React from 'react'
import {Link} from 'react-router-dom'

const Presentation = props =>(
    <div>
        <h1>HOME</h1>
        <Link to="/manageusers">Manage User</Link><br/>
        <Link to="/manageattendance">Manage Attendance</Link>
    </div>
)

export default Presentation