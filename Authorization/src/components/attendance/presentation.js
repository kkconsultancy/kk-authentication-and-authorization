import React from 'react'
import { Route, Switch, Link, Redirect} from 'react-router-dom';

import Add from './add'
import Edit from './edit'

const Presentation = props =>(
    <div className="mt-4">
        <div className="container card border-danger justify-content-center">
            {/* <h3 align="center" className="justify-content-center"> */}
                <div className="form-group text-center row badge-pill badge-warning">
                    {props.error}
                </div>
            {/* </h3> */}
            <form onSubmit={props.handleSubmit} className="form-inline justify-content-center">
                <div className="input-group my-1 mr-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text bg-danger text-white" htmlFor="course">COURSE</div>
                    </div>
                    <select className="custom-select my-1 mr-sm-2 " onChange={props.loadNewData} name="course">
                        {props.loadCourses}
                    </select>
                </div>
                {/* <label className="my-1 mr-2" htmlFor="course">BRANCH</label>
                <select className="custom-select my-1 mr-sm-2" onChange={props.loadNewData} name="branch">
                    {props.branchS}
                </select> */}
                <div className="input-group my-1 mr-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text bg-danger text-white" htmlFor="year">YEAR</div>
                    </div>
                    <select className="custom-select my-1 mr-sm-2" onChange={props.loadNewData} name="year">
                        {props.yearS}
                    </select>
                </div>
                {/* <label className="my-1 mr-2" htmlFor="year">SEMISTER</label>
                <select className="custom-select my-1 mr-sm-2" onChange={props.loadNewData} name="semister">
                    {props.semisterS}
                </select> */}                
                <div className="input-group my-1 mr-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text bg-danger text-white" htmlFor="section">SECTION</div>
                    </div>
                    <select className="custom-select my-1 mr-sm-2" onChange={props.loadNewData} name="section">
                        {props.sectionS}
                    </select>
                </div>
                <div className="input-group my-1 mr-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text bg-danger text-white">DATE</div>
                    </div>
                    <input disabled value={new Date().toISOString().substr(0, 10)} className="form-control bg-dark text-white" onChange={props.loadNewData} type="date" name="date"/>
                </div>
                {props.loading()} 
            </form>
        </div>      
        
        {(() => {
            const role = JSON.parse(localStorage.getItem(props.middleware.state.auth.currentUser.uid)).role
            if (props.script.state.rollNo && role === 'admin')
                return(
                    <div>
                        <Link to="/manageattendance/add">Add</Link><br />
                        <Link to="/manageattendance/edit">Edit</Link><br />
                    </div>
                )
        })()}
        <br/>
        {(() => {
            const role = JSON.parse(localStorage.getItem(props.middleware.state.auth.currentUser.uid)).role
            if (props.script.state.rollNo && role === 'admin')
                return (
                    <div>
                        <Switch>
                            <Route path="/manageattendance/add" component={() => <Add script={props.script} />} />
                            <Route path="/manageattendance/edit" component={() => <Edit script={props.script} />} />
                        </Switch>
                    </div>
                )
            else if (props.script.state.rollNo) return(
                <div>
                    <Switch>
                        <Route path="/manageattendance" component={() => <Add script={props.script} />} />
                        <Redirect from="**" to="/manageattendance"/>
                    </Switch>
                </div>
            )
        })()}
    </div>
)

export default Presentation