import React from 'react'

const Presentation = props =>(
    <div>
        <form onSubmit={props.handleSubmit}>
            <div className="container-fluid">
                <table className="table-responsive table-bordered">
                    <tbody>
                        <tr>
                            <th>Regd. Nos/Periods:</th>
                            {props.periodChecks()}
                        </tr>
                        {props.studsdata()}
                    </tbody>
                </table>
                <div className="form-group row">
                    <div className="col">
                        <input className="form-control btn btn-danger" type="submit" disabled={props.isDisabled} value="SUBMIT" onClick={props.handleSubmit}/>
                    </div>
                </div>
            </div>
        </form>
    </div>
)

export default Presentation
