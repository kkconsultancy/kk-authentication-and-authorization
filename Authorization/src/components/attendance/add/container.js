import React,{Component} from 'react'
import Presentation from './presentation';
// import Axios from 'axios';
// import Script from '../script';


class Container extends Component{
    script = null
    componentWillMount(){
        this.script = this.props.script
    }
    componentDidMount(){
        // console.log(this.props)
        this.setState({
            regnos: this.script.state.rollNo
        })
        // console.log(this.script.state.metadata, this.script.state.today)
        if(this.script.state.metadata)
        if (this.script.state.metadata[this.script.state.today]) {
            // this.setState({})
            // console.log(this.script.state.metadata[this.script.state.today])
            this.setState({
                disabledPeriods: this.script.state.metadata[this.script.state.today]
            })
        }
    }

    state={
        courseS : ["---","BTECH","MTECH","MBA","DIPLOMA"],
        isDisabled : true,
        regnos: ["16K61A05G7", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        checked : true,
        disabledPeriods : ''
    }
    loadCourses = this.state.courseS.map(c => {
        return (
            <option value={c} key={c}>{c}</option>
        )
    })
    loadNewData = (e) => {
        var data = {nodata : ''}
        data = this.script.newEvent(e.target.name, e.target.value)
        console.log(e.target.name)
        if(e.target.name.match(/subjectName[0-9]{1}/)){
            this.periodCheck({
                target : {
                    id : "checked-" + e.target.name.split("subjectName")[0]
                }
            })
        }
        if(data.branch){
            const b = data.branch.map(b => {
                return <option value={b} key={b}>{b}</option>
            })
            this.setState({
                branchS : b
            })
        }
        if (data.year) {
            const y = data.year.map(y => {
                return <option value={y} key={y}>{y}</option>
            })
            this.setState({
                yearS: y
            })
        }
        if (data.semister) {
            const s = data.semister.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                semisterS: s
            })
        }
        if (data.section) {
            const s = data.section.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                sectionS: s
            })
        }
        // if (data.rollNo){
        //     this.setState({
        //         regnos : data.rollNo
        //     })
        // }
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled : false
                })
    }
    checkedPeriods = {}
    periodCheck = (e) => {
        console.log(e)
        this.checkedPeriods[e.target.id.split('-')[1]] = this.checkedPeriods[e.target.id.split('-')[1]] ? false : "input-group-text bg-info"
        // console.log(this.checkedPeriods[e.target.id.split('-')[1]].toString())
        if (this.checkedPeriods[e.target.id.split('-')[1]]){
            e.target.className = "input-group-text bg-success text-white"
        }else{
            e.target.className = "input-group-text bg-danger text-white"
        }
        this.setState({
            checked : this.state.checked ? true : false,
        })
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled: false
                })
    }
    periodChecks = () => {
        // console.log(this.script.state)
        const pc = this.state.periods.map(p => {
            return(
                <th className="text-center justify-content-center" key={p.id}>
                    <div className="input-group my-1 mr-2">
                        <div className="input-group-prepend">
                            <div 
                                className={(this.state.disabledPeriods[p.id]) ? "input-group-text bg-dark text-white" :"input-group-text bg-danger text-white"} 
                                id={"checked-" + p.id} 
                                onClick={(this.state.disabledPeriods[p.id]) ? null : this.periodCheck} 
                                // {this.periodCheck}
                                >{p.id}</div>
                        </div>
                        {
                            (() => {
                                if (this.state.disabledPeriods[p.id])
                                    return <input type="text" className="custom-select my-1 mr-sm-2 "
                                        name={"subjectName" + p.id}
                                        value={this.state.disabledPeriods[p.id]}
                                        disabled={true}
                                    />
                                else
                                    return <input type="text" className="custom-select my-1 mr-sm-2 "
                                        onChange={this.loadNewData}
                                        name={"subjectName" + p.id}
                                    />
                            })()
                        }
                    </div>
                </th>
            )
        })
        return pc
    }
    checks = (regno) => {
        const c = this.state.periods.map(p => {
            return(
                <td className="justify-content-center" key={p.id}>
                    <span title={regno}>
                        <input className={"" + ((!this.checkedPeriods[p.id]) ? "input-group-text bg-dark" : this.checkedPeriods[p.id]) +""} type="button" disabled={!this.checkedPeriods[p.id]} onClick={this.handleAttendance} id={regno+"-"+p.id}/>
                    </span>
                </td>
            )
        })
        return c
    }
    studs = () => {
        const s = this.state.regnos.map(i=>{
            return(
                <tr key={i} align="center">                   
                    <td >{i}</td>
                    {this.checks(i)}
                </tr>
            )
        })
        return s
    }
    attendance = {}
    handleAttendance = (e) => {
        // console.log(e.target.id.split('-')[1])
        if(!this.attendance[e.target.id.split('-')[0]]){
            this.attendance[e.target.id.split('-')[0]] = {}
        }
        // const data = {
        //     Rollno: "16K61A05G7",
        //     SubjectName: "",
        //     PeridNo: "1",
        //     MONTHYEAR: "",
        //     Date: ""
        // }
        this.attendance[e.target.id.split('-')[0]][e.target.id.split('-')[1]] = this.attendance[e.target.id.split('-')[0]][e.target.id.split('-')[1]] ? false : true
        if (this.attendance[e.target.id.split('-')[0]][e.target.id.split('-')[1]]) {
            e.target.className = "input-group-text bg-danger text-white"
        } else {
            e.target.className = "input-group-text bg-info text-white"
        }
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled: false
                })
    }
    handleChange = (e) => {
        // if(e.target.id === "regnos")
        // console.log(e)
        if(e.code){
            this.props.callback(e);
            return;
        }
        
        // if (e.target.id === "regnos"){
        //     this.attendance = {}
        //     this.checkedPeriods = {}
        //     this.periodChecks()
        // }
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isDisabled : true
        })
        // console.log(this.state)
        this.script.newEvent("attendance",this.attendance);
        this.script.newEvent("checkedPeriods",this.checkedPeriods);
        // this.setState({
        //     attendance : this.attendance,
        //     checkedPeriods : this.checkedPeriods
        // })
        this.script.handleSubmit(e)
    }

    render(){
        
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                studsdata={this.studs}
                periodChecks={this.periodChecks}
                loadCourses={this.loadCourses}
                loadNewData={this.loadNewData}
            />
        )        
    }
}

export default Container