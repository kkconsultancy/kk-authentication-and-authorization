import React,{Component} from 'react'
import Presentation from './presentation';
import Script from './script';

class Container extends Component{
    script = null
    componentWillMount(){
        const user = this.props.middleware.state.auth.currentUser;
        user.firebaseUrl = this.props.middleware.firebaseUrl
        this.script = new Script(user, null);
    }
    componentDidMount(){
        // console.log(this.props)
        this.script.callback = this.callback
    }
    state={
        courseS : ["---","BTECH","MTECH","MBA","DIPLOMA"],
        isDisabled : true,
        regnos: ["16K61A05G7", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        checked : true,
        countDown : 10,
    }
    loadCourses = this.state.courseS.map(c => {
        return (
            <option value={c} key={c}>{c}</option>
        )
    })
    loadNewData = (e) => {
        var data = {nodata : ''}
        data = this.script.newEvent(e.target.name, e.target.value)
        if(data.branch){
            const b = data.branch.map(b => {
                return <option value={b} key={b}>{b}</option>
            })
            this.setState({
                branchS : b
            })
        }
        if (data.year) {
            const y = data.year.map(y => {
                return <option value={y} key={y}>{y}</option>
            })
            this.setState({
                yearS: y
            })
        }
        if (data.semister) {
            const s = data.semister.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                semisterS: s
            })
        }
        if (data.section) {
            const s = data.section.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                sectionS: s
            })
        }
    }
    handleChange = (e) => {
        // console.log(e)
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    callback = (response) => {
        if(response === "POSTED"){
            var countDown = setInterval(() => {
                this.setState({
                    error: "ATTENDANCE HAS BEEN POSTED SUCESSFULLY PLEASE WAIT "+this.state.countDown+"s"
                })
                this.setState({
                    countDown : this.state.countDown - 1
                })
                
                console.log(this.state.countDown,'here')
                if(this.state.countDown === 1){
                    this.loadNewData({
                        target:{
                            name:"nodata", 
                            value:"reload"
                        }
                    })
                    this.setState({
                        countDown : 5
                    })
                    clearInterval(countDown)
                    return;
                }
            },1000);
            return;
        }
        this.setState({
            error: "",
            loading: false
        })
        // console.log(response)
        if(response.code){
            this.setState({ 
                error: response.code.split('/')[1] + " : " + response.message,
                loading : false
            })
        }else if(response.target){
            if(response.target.value)
                this.setState({
                    rollNo: response.target.value,
                    loading : false
                })
            else
                this.setState({
                    error: "NO DATA HAS BEEN FOUND FOR THE GIVEN FILTERS",
                    loading: false
                })
        }
        if (response.asyncQuery) {
            this.setState({
                loading: true
            })
        }
    }

    loading = () => {
        if(this.state.loading){
            return(
                <div className="spinner-border" role="status">
                    <span></span>
                </div>
            )
        }else
            return <div></div>
    }

    render(){
        
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                studsdata={this.studs}
                periodChecks={this.periodChecks}
                loadCourses={this.loadCourses}
                loadNewData={this.loadNewData}
                middleware={this.props.middleware}
                script={this.script}
                callback={this.callback}
                loading={this.loading}
            />
        )        
    }
}

export default Container