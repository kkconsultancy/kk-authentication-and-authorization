import React from 'react'

const Presentation = props =>(
    <div>
        <form onSubmit={props.handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <th>Regd. Nos/Periods:</th>
                        {props.periodChecks()}
                    </tr>
                    {props.studsdata()}
                </tbody>
            </table>
            <input type="submit" disabled={props.isDisabled} value="Update" onClick={props.handleSubmit}/>
        </form>
        
    </div>
)

export default Presentation
