import React,{Component} from 'react'
import Presentation from './presentation';

class Container extends Component{
    script = null
    componentDidMount(){
        console.log(this.props)
        this.script = this.props.script
        this.setState({
            regnos: this.script.state.rollNo
        })
    }
    state={
        courseS : ["---","BTECH","MTECH","MBA","DIPLOMA"],
        isDisabled : true,
        regnos: ["16K61A05G7", "16K61A0559", "16K61A05A3","16K61A05D5"],
        periods : [{id : 1},{ id : 2},{id:3},{id: 4},{id:5},{id:6},{id:7}],
        checked : true
    }
    loadCourses = this.state.courseS.map(c => {
        return (
            <option value={c} key={c}>{c}</option>
        )
    })
    loadNewData = (e) => {
        var data = {nodata : ''}
        data = this.script.newEvent(e.target.name, e.target.value)
        if(data.branch){
            const b = data.branch.map(b => {
                return <option value={b} key={b}>{b}</option>
            })
            this.setState({
                branchS : b
            })
        }
        if (data.year) {
            const y = data.year.map(y => {
                return <option value={y} key={y}>{y}</option>
            })
            this.setState({
                yearS: y
            })
        }
        if (data.semister) {
            const s = data.semister.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                semisterS: s
            })
        }
        if (data.section) {
            const s = data.section.map(s => {
                return <option value={s} key={s}>{s}</option>
            })
            this.setState({
                sectionS: s
            })
        }
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled : false
                })
    }
    checkedPeriods = {}
    periodCheck = (e) => {
        this.checkedPeriods[e.target.id.split('-')[1]] = this.checkedPeriods[e.target.id.split('-')[1]] ? false : true
        this.setState({
            checked : this.state.checked ? true : false
        })
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled: false
                })
    }
    periodChecks = () => {
        const pc = this.state.periods.map(p => {
            return(
                <th key={p.id}>
                    {p.id}<br/>
                    <input type="text" onChange={this.loadNewData} name={"subjectName"+p.id} /><br/>
                    <input type="checkbox" id={"checked-"+p.id} onChange={this.periodCheck}/>
                </th>
            )
        })
        return pc
    }
    checks = (regno) => {
        const c = this.state.periods.map(p => {
            return(
                <td key={p.id}>
                    <input type="checkbox" onChange={this.handleAttendance} disabled={!this.checkedPeriods[p.id]} id={regno+"-"+p.id} value={regno+"-"+p.id}/>
                </td>
            )
        })
        return c
    }
    studs = () => {
        const s = this.state.regnos.map(i=>{
            return(
                <tr key={i} align="center">                   
                    <td>{i}</td>
                    {this.checks(i)}
                </tr>
            )
        })
        return s
    }
    attendance = {}
    handleAttendance = (e) => {
        console.log(e.target.id.split('-')[1])
        if(!this.attendance[e.target.id.split('-')[0]]){
            this.attendance[e.target.id.split('-')[0]] = {}
        }
        this.attendance[e.target.id.split('-')[0]][e.target.id.split('-')[1]] = this.attendance[e.target.id.split('-')[0]][e.target.id.split('-')[1]] ? false : true
        if (Object.keys(this.attendance).length)
            if (Object.keys(this.checkedPeriods).length)
                this.setState({
                    isDisabled: false
                })
    }
    handleChange = (e) => {
        console.log(e)
        if(e.code){
            this.props.callback(e);
            return;
        }
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
        this.script.newEvent("attendance",this.attendance);
        this.script.newEvent("checkedPeriods",this.checkedPeriods);
        this.script.handleSubmit(e)
    }

    render(){
        
        return(
            <Presentation
                {...this.state}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
                studsdata={this.studs}
                periodChecks={this.periodChecks}
                loadCourses={this.loadCourses}
                loadNewData={this.loadNewData}
            />
        )        
    }
}

export default Container