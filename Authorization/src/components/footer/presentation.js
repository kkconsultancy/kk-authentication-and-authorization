import React from 'react'

const Presentation = props =>(
    <footer className="page-footer font-small bg-danger pt-4 sticky-bottom">
        <div className="container-fluid text-center text-white text-md-left">
            <div className="row">
                <div className="col-md-6 mt-md-0 mt-3">
                <h5 className="text-uppercase">SITE Automation</h5>
                <p>Portal for Attendance Management in respective Departments.</p>
                </div>
            </div>
        </div>
        <div className="footer-copyright text-center text-white py-3">©
        <a className="text-white" href="https://developerswork.online" target="_blank" rel="noopener noreferrer">
            <img src="https://i0.wp.com/developerswork.online/wp-content/uploads/2019/03/DESIGNS-Copy.png?fit=360%2C60&amp;ssl=1" alt="developer"/>
        </a>
        </div>
    </footer>
)

export default Presentation