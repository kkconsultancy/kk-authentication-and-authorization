import React from 'react'

const Presentation = props => (
    <div className="container">
        <div className="row d-flex p-2">
            <div className="col-sm-4"></div>
            <form onSubmit={props.handleSubmit} className="col-sm-4"  method="POST">
                <div className="form-group row badge badge-pill badge-warning">
                    {props.error}
                </div>
                <div className="form-group row">
                    <label htmlFor="username" >Username: *</label>
                    <input 
                        type="text"
                        disabled={props.buttonDisabled}
                        className="form-control border-danger" 
                        placeholder="Email" 
                        id="username" 
                        name="username"
                        onChange={props.handleUpdate}
                    />
                </div>
                <div className="form-group row">
                    <label htmlFor="password" >Password: *</label>
                    <input 
                        type="password" 
                        disabled={props.buttonDisabled}
                        className="form-control border-danger"  
                        placeholder="*******"
                        id="password" 
                        name="password"
                        onChange={props.handleUpdate}
                    />
                </div>
                <div className="form-group row">
                    <div className="col">
                        <button 
                            type="submit" 
                            disabled={props.buttonDisabled}
                            className="btn btn-danger btn-block" 
                            onClick={props.handleSubmit}>
                            Login
                        </button>
                    </div>
                    <div className="col">
                        <button
                            disabled={props.buttonDisabled} 
                            type="reset" 
                            className="btn btn-danger btn-block">
                            Cancel
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
)

export default Presentation