import React,{Component} from 'react';
import Presentation from './presentation';

class Container extends Component{
    
    state = {
        buttonDisabled: false,
    }

    handleUpdate = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            buttonDisabled : true
        })
        if(this.state.username && this.state.password)
            this.props.firebase.signin({email:this.state.username,password:this.state.password},(res)=>{
                // console.log(res)
                this.setState({
                    buttonDisabled : false
                })
                if(res.error){
                    this.setState({error: res.code.split('/')[1].toUpperCase()})
                }
            })
        else    this.setState({error: "Enter Username and Password!".toUpperCase(),buttonDisabled : false})
    }
    render(){
        return(

            <Presentation
                {...this.state}
                handleSubmit={this.handleSubmit}
                handleUpdate={this.handleUpdate}
            />
        )
    }
}

export default Container;