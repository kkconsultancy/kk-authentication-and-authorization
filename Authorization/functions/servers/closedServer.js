
class ClosedServer {
    constructor(app, firebase) {
        this.app = app
        this.firebase = firebase
    
        this.app.post('/getServices', (request, response) => {
            const urlAttr = request.originalUrl.split('/')
            const data = {
                uid: urlAttr[2],
                data: request.body
            }
            // console.log(data)
            this.firebase.auth().getUser(data.uid)
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord.
                // console.log('Successfully fetched user data:', userRecord.toJSON());
                // console.log(userRecord.toJSON())
                return this.firebase.firestore().collection('PROFILES').doc(userRecord.uid).get()
            }).then(res => {
                // console.log(res.data())
                const role = res.data().role
                return this.firebase.firestore().collection('SERVICES').doc(role).get()
            }).then(res => {
                // console.log(res.data())
                const data = res.data()
                for(let i in data){
                    for(let j in data[i]){
                        data[i][j] = {
                            name : j.toUpperCase(),
                            route : j.toLowerCase()
                        }
                    }
                    data[i] = {
                        childs : data[i],
                        route : i.toLowerCase(),
                        name : i.toUpperCase()
                    }
                }
                response.send(data)
            })
            .catch((err) =>{
                // console.log('Error fetching user data:', err);
                response.send(err)
                
            });
        });

        this.app.post('/getAttendanceProfiles', (req,res) => {
            const urlAttr = req.originalUrl.split('/')
            const data = {
                uid: urlAttr[2],
                data: req.body
            }
            this.firebase.auth().getUser(data.uid)
            .then(function (userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                // console.log('Successfully fetched user data:', userRecord.toJSON());
                // res.send(userRecord.toJSON());
                data.userRecord = userRecord
                this.firebase.firestore().collection('METADATA/' + data.data.academicYear + '/' + data.data.course+'').doc(""+data.data.branch+"").get()
                .then(result => {
                    console.log(result.data())
                    data.result = result.data()
                    res.send(data)
                }).catch(err => {
                    res.send(err)
                })
            }).catch(function (err) {
                // console.log('Error fetching user data:', err);
                res.send(err)
            });
        })
    }
}

module.exports = ClosedServer;