
class PrivilegedServer {
    constructor(app,firebase) {
        this.app = app
        this.firebase = firebase
    
        this.app.post('/postAttendance',(request,response)=>{
            var error = new Error("NO ERROR")
            const urlAttr = request.originalUrl.split('/')
            const uid = urlAttr[2]
            const idToken = urlAttr[3]
            const arrayData = []
            const splitedDate = request.body.date.split('-')
            const present = new Date().toISOString().split('-');
            const MongoClient = require('mongodb').MongoClient;
            const url = "mongodb://admin:site123@ds141641.mlab.com:41641/sasi_attendance";
            var dbo = null;
            // const date = '16/06/2019 ' + present[2].split('T')[1].split('.')[0]
            const date = splitedDate[1] + '/' + splitedDate[2] + '/' + splitedDate[0] + ' ' + present[2].split('T')[1].split('.')[0]
            const fdate = splitedDate[2] + '/' + splitedDate[1] + '/' + splitedDate[0] + ' ' + present[2].split('T')[1].split('.')[0]
            MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
                if (err){ 
                    console.log(err);
                    error = err;
                }
                if(!db){
                    response.send({
                        "code": "network/connection-failed",
                        "message": "There is problem with connecting to our servers now, try again later."
                    })
                    throw error;
                }
                // console.log('MLAB CONNECTION SUCESSFUL')
                dbo = db.db("sasi_attendance");
                this.firebase.auth().verifyIdToken(idToken)
                    .then((decodedToken) => {
                        const uidToken = decodedToken.uid;
                        if (uidToken === uid) {
                            return this.firebase.firestore().runTransaction(t => {
                                for (let i in request.body.attendance) {
                                    const firebase = {}
                                    for (let j in request.body.checkedPeriods) {
                                        if(!request.body.checkedPeriods[j])
                                            continue
                                        if(!request.body.attendance[i][j])
                                            continue
                                        const data = {
                                            Rollno: i,
                                            SubjectName: request.body["subjectName" + j],
                                            PeridNo: j,
                                            MONTHYEAR: parseInt(splitedDate[1]) + '/' + splitedDate[0],
                                            Date: date
                                        }
                                        firebase[j] = request.body["subjectName" + j]
                                        // console.log(data)
                                        try{    
                                            
                                            // if (err) return Promise.reject(err);
                                            // var dbo = db.db("sasi_attendance");
                                            arrayData[arrayData.length] = Promise.resolve(dbo.collection("ATTENDANCE").insertOne(data, function (err, res) {
                                                if (err) return Promise.reject(err);
                                                // console.log("1 document inserted");
                                                // console.log(res.result);
                                                // db.close();
                                                return res.result;
                                                // return Promise.resolve(res.result);
                                            }))
                                        }catch(err){
                                            return Promise.reject(err);
                                        }
                                    }
                                    try {
                                        
                                        var collection = "ATTENDANCE/" + request.body.academicYear + "/"
                                        collection += request.body.course + "/" + request.body.branch + "/" + request.body.year + "/" + request.body.section + "/"
                                        var c = collection + i
                                        const document = splitedDate[1] + '-' + splitedDate[0]
                                        var ref = this.firebase.firestore().collection(c).doc(document)
                                        arrayData[arrayData.length] = t.set(ref, {
                                            [fdate.split(' ')[0].replace('/', '-').replace('/', '-')]: firebase
                                        }, { merge: true });
                                    } catch (err) {
                                        return Promise.reject(err);
                                    }
                                }
                              	collection = "ACADEMICS/" + request.body.academicYear + "/"
                                collection += request.body.course + "/" + request.body.branch + "/" + request.body.year
                                ref = this.firebase.firestore().collection(collection).doc(request.body.section)
                              	const metadata = {} 
                                for(let i=1;i<=8;i++){
                                	if(request.body["subjectName" + i]){
                                      metadata[i] = request.body["subjectName" + i];
                                    }
                                }
                                console.log(collection,request.body.section, metadata)
                              	arrayData[arrayData.length] = t.set(ref, {
                                	"metadata" :{
                                		[fdate.split(' ')[0].replace('/', '-').replace('/', '-')] : metadata
                                	}
                              	}, { merge: true });
                                return Promise.all(arrayData);
                            }).then(result => {
                                console.log('Transaction success!');
                                response.status(200).send(result)
                            }).catch(err => {
                                console.log('Transaction failure :'+err);
                                response.send({
                                    code : 'firestore/transaction-failed',
                                    message : 'transaction failed with an error',
                                    error : err
                                })
                            });
                        } else {
                            response.send({
                                "code": "auth/id-token-expired",
                                "message": "Firebase ID token has expired. Get a fresh ID token from your client app and try again (auth/id-token-expired). See https://firebase.google.com/docs/auth/admin/verify-id-tokens for details on how to retrieve an ID token."
                            })
                        }
                    }).catch(function (err) {
                        response.send(err)
                    });
            });
        })

        this.app.post('/getRollNo',(request,response)=>{
            const urlAttr = request.originalUrl.split('/')
            const uid = urlAttr[2]
            const idToken = urlAttr[3]
            console.log(urlAttr)
            // console.log(this.firebase.firestore.FieldValue.serverTimestamp())
            const academicYear = request.body.academicYear
            const course = request.body.course
            const branch = request.body.branch
            const year = request.body.year
            const section = request.body.section

            const collection = "ACADEMICS/"+academicYear+"/"+course+"/"+branch+"/"+year
            const document = section
            // console.log(idToken)
            this.firebase.auth().verifyIdToken(idToken)
                .then( (decodedToken) => {
                    // console.log(decodedToken)
                    const uidToken = decodedToken.uid;
                    if(uidToken === uid){
                        this.firebase.firestore().collection(collection).doc(document).get()
                            .then(res => {
                                // console.log(res)
                                const output = []
                                for (let i in res.data())
                                    if(i !== "metadata")
                                        output.push(i);
                                response.send({regNos : output.sort(),metadata : res.data()["metadata"]})
                            }).catch(err => {
                                response.send(err)
                            })
                    }else{
                        response.send({
                            "code": "auth/id-token-expired",
                            "message": "Firebase ID token has expired. Get a fresh ID token from your client app and try again (auth/id-token-expired). See https://firebase.google.com/docs/auth/admin/verify-id-tokens for details on how to retrieve an ID token."
                        })
                    }
                }).catch(function (err) {
                    console.log(idToken)
                    response.send(err)
                });
        })
    }
}

module.exports = PrivilegedServer