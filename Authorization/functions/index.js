const functions = require('firebase-functions');
const express = require('express')
const cors = require('cors')({ origin: true })
const admin = require('firebase-admin');
const bodyParser = require('body-parser');

admin.initializeApp(functions.config().firebase)

const app = express()
const openServer = express()
const closedServer = express()
const privilegedServer = express()
openServer.use(cors);
closedServer.use(cors);
privilegedServer.use(cors);

var server = require('./servers/privilegedServer')
new server(privilegedServer, admin)
server = require('./servers/closedServer')
new server(closedServer, admin)
server = require('./servers/openServer')
new server(openServer, admin)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors);
app.use('/v1', openServer);
app.use('/v1/:user', closedServer);
app.use('/v1/:user/:token', privilegedServer);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

exports.openAPI = functions.https.onRequest(app)
exports.closedAPI = functions.https.onRequest(app)
exports.privilegedAPI = functions.https.onRequest(app)


// Doesn't work
exports.syncMLab = functions.firestore
.document('ATTENDACE/{academicYear}/{course}/{branch}/{year}/{section}/{uid}/{month}')
.onCreate((change,context) => {
    console.log(change)
    const data = change.after.data()
    const MongoClient = require('mongodb').MongoClient;
    const url = "mongodb://suresh554:16k61a0554@ds141641.mlab.com:41641/sasi_attendance";
    // MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
    //     if (err) return Promise.reject(err);
    //     var dbo = db.db("sasi_attendance");
    //     dbo.collection("attendance2019").insertOne(data, function (err, res) {
    //         if (err) return Promise.reject(err);
    //         // console.log("1 document inserted");
    //         console.log(res);
    //         db.close();
    //         return Promise.resolve(res);
    //     })
    // });
    return Promise.resolve(url);
})

exports.intialiseNewUser = functions.firestore.document("PROFILES/{user}").onCreate((change,context) => {
    const raw = change.data()
    const data = {
        displaName: raw.Name,
        uid : raw.uid,
        email : raw.Email,
        password : "Sasi@123"
    }
    return admin.auth().createUser(data).then(user=>{
        console.log(user)
        return Promise.resolve(user)
    }).catch(err => {
        console.log(err)
        return Promise.reject(err)
    })
    
});

exports.getFirebaseConfig = functions.https.onRequest((request, response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body }
    cors(request, response, () => {
        if (conditions.type !== 'POST') {
            console.log(conditions);
            response.status(200).send('HOW ON EARTH YOU EXISTS...');
            return;
        }
        if (conditions.parameters.password !== "please dont ask me") {
            console.log(conditions);
            response.status(200).send('LANGUAGE WILL BE BADASS NOW :)');
            return;
        }
        console.log('EVERYTHING IS FINE!')
        const client = require('./json/client.json');
        response.status(200).send(client);
    });
});
