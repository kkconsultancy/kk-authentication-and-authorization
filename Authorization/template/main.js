var admin = require("firebase-admin");
var csv = require('csvtojson');
const fs = require('fs');

var serviceAccount = 'serviceAccount.json'

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://redants4site.firebaseio.com"
  });


var db = admin.firestore();

const p17cse = 'profiles/profile 2017/profiles/CSEPROFILE2017.csv';
const p17ece = 'profiles/profile 2017/profiles/ECEPROFILE2017.csv';
const p17eee = 'profiles/profile 2017/profiles/EEEPROFILE2017.csv';
const p17it = 'profiles/profile 2017/profiles/ITPROFILE2017.csv';
const p17mech = 'profiles/profile 2017/profiles/MECHPROFILE2017.csv';
const p17pt = 'profiles/profile 2017/profiles/PTPROFILE2017.csv';
const p17civil = 'profiles/profile 2017/profiles/CSEPROFILE2017.csv';


const p15cse = 'profiles/profiles 2015/profiles/CSEPROFILE2015.csv';
const p15ece = 'profiles/profiles 2015/profiles/ECEPROFILE2015.csv';
const p15eee = 'profiles/profiles 2015/profiles/EEEPROFILE2015.csv';
const p15it = 'profiles/profiles 2015/profiles/ITPROFILE2015.csv';
const p15mech = 'profiles/profiles 2015/profiles/MECHPROFILE2015.csv';
const p15civil = 'profiles/profiles 2015/profiles/CIVILPROFILE2015.csv';

const m17cse = 'profiles/profile 2017/marks/CSEmarks2017.csv';
const m17ece = 'profiles/profile 2017/marks/ECEmarks2017.csv';
const m17eee = 'profiles/profile 2017/marks/EEEmarks2017.csv';
const m17it = 'profiles/profile 2017/marks/ITmarks2017csv';
const m17mech = 'profiles/profile 2017/marks/MEmarks2017.csv';
const m17pt = 'profiles/profile 2017/marks/PEmarks2017csv';
const m17civil = 'profiles/profile 2017/marks/civilmarks2017.csv';

const phn = 'profiles/phn no.csv';

const arr =[p17cse,p17ece,p17eee,p17it,p17mech,p17pt,p17civil];
const arrmarks17 =[m17cse,m17ece,m17eee,m17it,m17mech,m17pt,m17civil];

const arrp15 = [p15cse,p15ece,p15eee,p15it,p15mech,p15civil];



//inserting phone numbers sample into firebase


// csv()
// .fromFile(phnsample)
// .then((jsonObj) => {
      
//     var docRef = db.collection('phonenum').doc('phnsample');

//     var data={};

//     for(i in jsonObj){
//         data[jsonObj[i].regno] = jsonObj[i];
//         data[jsonObj[i].regno].uid = jsonObj[i].regno;
//         // console.log(data);
//     }
//     docRef.set(data).catch(err =>
//         console.log(err)
//     )

// })

// db.collection("phonenumbers").doc("phn").delete().then(function() {
//   console.log("Document successfully deleted!");
// }).catch(function(error) {
//   console.error("Error removing document: ", error);
// });



//inserting profils of 2017-2021 batch into firebase
// for(i in arr){
//     console.log(arr[i]);

// csv()
// .fromFile(arr[i])
// .then((jsonObj) => {
//     const batch = "2017-2021";
//     const program = "BTECH"    
//     var docRef = db.collection('profiles/'+program+'/'+batch).doc(jsonObj[0].Branch);

//     var data={};

//     for(i in jsonObj){
//         data[jsonObj[i].regno] = jsonObj[i];
//         data[jsonObj[i].regno].uid = jsonObj[i].regno;
//         data[jsonObj[i].regno].batch = batch;
//         data[jsonObj[i].regno].program = program;
//         // console.log(data);
//     }
//     docRef.set(data).catch(err =>
//         console.log(err)
//     )

// })
// }


// inserting marks of 2017-2021 batch CSE Branch into firebase
// csv()
// .fromFile(m17cse)
// .then((jsonObj) => {
//     const batch = "2017-2021";
//     const program = "BTECH";
//     const branch = "CSE";

//     var data={};
//     let n = 0;
//     var interval = setInterval(function(){
//       let i = n++;
//       data = {}
//       if(!jsonObj[i]){
//         console.log('completed');
//         clearInterval(interval)
//         return;
//       }
//       // data[jsonObj[i].regno] = jsonObj[i];
//       // console.log(data);
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM11");
//       data = {};
//       data = jsonObj[i].sem11 ? jsonObj[i].sem11 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.branch = branch;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM12");
//       data = {};
//       data = jsonObj[i].sem12 ? jsonObj[i].sem12 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM21");
//       data = {};
//       data = jsonObj[i].sem21 ? jsonObj[i].sem21 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM22");
//       data = {};
//       data = jsonObj[i].sem22 ? jsonObj[i].sem22 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM31");
//       data = {};
//       data = jsonObj[i].sem31 ? jsonObj[i].sem31 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM32");
//       data = {};
//       data = jsonObj[i].sem32 ? jsonObj[i].sem32 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM41");
//       data = {};
//       data = jsonObj[i].sem41 ? jsonObj[i].sem41 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM42");
//       data = {};
//       data = jsonObj[i].sem42 ? jsonObj[i].sem42 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//     },1000);
    
//   //  console.log(jsonObj[0])

// })





// inserting marks of 2017-2021 batch ECE Branch into firebase
// csv()
// .fromFile(m17ece)
// .then((jsonObj) => {
//     const batch = "2017-2021";
//     const program = "BTECH";
//     const branch = "ECE";

//     var data={};
//     let n = 0;
//     var interval = setInterval(function(){
//       let i = n++;
//       data = {}
//       if(!jsonObj[i]){
//         console.log('completed');
//         clearInterval(interval)
//         return;
//       }
//       // data[jsonObj[i].regno] = jsonObj[i];
//       // console.log(data);
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM11");
//       data = {};
//       data = jsonObj[i].sem11 ? jsonObj[i].sem11 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.branch = branch;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM12");
//       data = {};
//       data = jsonObj[i].sem12 ? jsonObj[i].sem12 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM21");
//       data = {};
//       data = jsonObj[i].sem21 ? jsonObj[i].sem21 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM22");
//       data = {};
//       data = jsonObj[i].sem22 ? jsonObj[i].sem22 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM31");
//       data = {};
//       data = jsonObj[i].sem31 ? jsonObj[i].sem31 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM32");
//       data = {};
//       data = jsonObj[i].sem32 ? jsonObj[i].sem32 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM41");
//       data = {};
//       data = jsonObj[i].sem41 ? jsonObj[i].sem41 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM42");
//       data = {};
//       data = jsonObj[i].sem42 ? jsonObj[i].sem42 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//     },1000);
//   //  console.log(jsonObj[0])
// })



// inserting marks of 2017-2021 batch Civil Branch into firebase

// csv()
// .fromFile(m17civil)
// .then((jsonObj) => {
//     const batch = "2017-2021";
//     const program = "BTECH";
//     const branch = "CIVIL";

//     var data={};
//     let n = 0;
//     var interval = setInterval(function(){
//       let i = n++;
//       data = {}
//       if(!jsonObj[i]){
//         console.log('completed');
//         clearInterval(interval)
//         return;
//       }
//       // data[jsonObj[i].regno] = jsonObj[i];
//       // console.log(data);
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM11");
//       data = {};
//       data = jsonObj[i].sem11 ? jsonObj[i].sem11 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.branch = branch;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM12");
//       data = {};
//       data = jsonObj[i].sem12 ? jsonObj[i].sem12 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM21");
//       data = {};
//       data = jsonObj[i].sem21 ? jsonObj[i].sem21 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM22");
//       data = {};
//       data = jsonObj[i].sem22 ? jsonObj[i].sem22 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM31");
//       data = {};
//       data = jsonObj[i].sem31 ? jsonObj[i].sem31 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM32");
//       data = {};
//       data = jsonObj[i].sem32 ? jsonObj[i].sem32 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM41");
//       data = {};
//       data = jsonObj[i].sem41 ? jsonObj[i].sem41 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM42");
//       data = {};
//       data = jsonObj[i].sem42 ? jsonObj[i].sem42 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//     },1000);
//   //  console.log(jsonObj[0])
// })

// inserting marks of 2017-2021 batch EEE Branch into firebase

// csv()
// .fromFile(m17eee)
// .then((jsonObj) => {
//     const batch = "2017-2021";
//     const program = "BTECH";
//     const branch = "EEE";

//     var data={};
//     let n = 0;
//     var interval = setInterval(function(){
//       let i = n++;
//       data = {}
//       if(!jsonObj[i]){
//         console.log('completed');
//         clearInterval(interval)
//         return;
//       }
//       // data[jsonObj[i].regno] = jsonObj[i];
//       // console.log(data);
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM11");
//       data = {};
//       data = jsonObj[i].sem11 ? jsonObj[i].sem11 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.branch = branch;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM12");
//       data = {};
//       data = jsonObj[i].sem12 ? jsonObj[i].sem12 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM21");
//       data = {};
//       data = jsonObj[i].sem21 ? jsonObj[i].sem21 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM22");
//       data = {};
//       data = jsonObj[i].sem22 ? jsonObj[i].sem22 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM31");
//       data = {};
//       data = jsonObj[i].sem31 ? jsonObj[i].sem31 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM32");
//       data = {};
//       data = jsonObj[i].sem32 ? jsonObj[i].sem32 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM41");
//       data = {};
//       data = jsonObj[i].sem41 ? jsonObj[i].sem41 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//       var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM42");
//       data = {};
//       data = jsonObj[i].sem42 ? jsonObj[i].sem42 : {}
//       data.uid = jsonObj[i].regno;
//       data.batch = batch;
//       data.course = program;
//       data.regno = jsonObj[i].regno;
//       docRef.set(data).catch(err =>
//         console.log(err)
//       );
//     },1000);
//   //  console.log(jsonObj[0])
// })



// inserting marks of 2017-2021 batch Mech  Branch into firebase

csv()
.fromFile(m17mech)
.then((jsonObj) => {
    const batch = "2017-2021";
    const program = "BTECH";
    const branch = "MECH";

    var data={};
    let n = 0;
    var interval = setInterval(function(){
      let i = n++;
      data = {}
      if(!jsonObj[i]){
        console.log('completed');
        clearInterval(interval)
        return;
      }
      // data[jsonObj[i].regno] = jsonObj[i];
      // console.log(data);
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM11");
      data = {};
      data = jsonObj[i].sem11 ? jsonObj[i].sem11 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.branch = branch;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM12");
      data = {};
      data = jsonObj[i].sem12 ? jsonObj[i].sem12 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM21");
      data = {};
      data = jsonObj[i].sem21 ? jsonObj[i].sem21 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM22");
      data = {};
      data = jsonObj[i].sem22 ? jsonObj[i].sem22 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM31");
      data = {};
      data = jsonObj[i].sem31 ? jsonObj[i].sem31 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM32");
      data = {};
      data = jsonObj[i].sem32 ? jsonObj[i].sem32 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM41");
      data = {};
      data = jsonObj[i].sem41 ? jsonObj[i].sem41 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
      var docRef = db.collection('marks/'+program+'/'+batch).doc(branch+"/"+jsonObj[i].regno+"/SEM42");
      data = {};
      data = jsonObj[i].sem42 ? jsonObj[i].sem42 : {}
      data.uid = jsonObj[i].regno;
      data.batch = batch;
      data.course = program;
      data.regno = jsonObj[i].regno;
      docRef.set(data).catch(err =>
        console.log(err)
      );
    },1000);
  //  console.log(jsonObj[0])
})




//inserting profils of 2015-2019 batch into firebase

// for(i in arrp15){
//     console.log(arrp15[i]);

// csv()
// .fromFile(arrp15[i])
// .then((jsonObj) => {
//     const batch = "2015-2019";
//     const program = "BTECH"    
//     var docRef = db.collection('profiles/'+program+'/'+batch).doc(jsonObj[0].Branch);

//     var data={};

//     for(i in jsonObj){
//         data[jsonObj[i].regno] = jsonObj[i];
//         data[jsonObj[i].regno].uid = jsonObj[i].regno;
//         data[jsonObj[i].regno].batch = batch;
//         data[jsonObj[i].regno].program = program;
//         // console.log(data);
//     }
//     docRef.set(data).catch(err =>
//         console.log(err)
//     )

// })
// }






