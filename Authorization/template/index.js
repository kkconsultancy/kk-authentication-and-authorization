const express = require('express');
const bodyParser = require('body-parser');
var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://redants4site.firebaseio.com"
});

const app = express();
app.listen(3000)
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static('./public'))
app.use(bodyParser.json());

app.post('/setup',(req,res) => {
	const data = {
        password : req.body.password,
        data: req.body
    }
    console.log('METADATA/' + data.data.academicYear + '/' + data.data.course + '')
    admin.firestore().collection('METADATA/' + data.data.academicYear + '/' + data.data.course + '').doc("" + data.data.branch + "").get()
    .then(result => {
        console.log('started')
        // result.forEach(function (doc) {
        //     // doc.data() is never undefined for query doc snapshots
        //     console.log(doc.id, " => ", doc.data());
        // });
        res.send(result.data())
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
    // admin.database().ref('/password').once('value')
    // .then(result => {
    //     if(result.val() === data.password){
    //         const template = require('./template.json');
    //         const authorisation = admin.firestore().collection("authorisation")

    //         authorisation.doc('services').set(template.services)
    //         .then(result => {
    //             for(let i in template.services){
    //                 for(let j in template.services[i].operations){
    //                     const string = 'services/'+i+'/'+j;
    //                     template.roles.admin[i][j] = authorisation.doc(string)
    //                 }
    //             }
    //             authorisation.doc('roles').set(template.roles);
    //             res.status(200).send('COMPLETED');
    //         }).catch(err => {
    //             console.log(err)
    //             res.status(500).send(err);
    //         })
    //     }else
    //         res.status(200).send("SORRY NO TIME NOW.")
    // }).catch(err => {
    //     console.log(err)
    //     res.status(200).send(err)
    // })
})