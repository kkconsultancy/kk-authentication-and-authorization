const csv = require('csvtojson');
const admin = require('firebase-admin');
admin.initializeApp({
    credential: admin.credential.cert(require('./serviceAccountKey.json')),
    databaseURL: "https://redants4site.firebaseio.com"
});
var db = admin.firestore();

// const profiles = ["2016-2020.BTECH.profiles.csv"]
const profiles = ["2018-2022.BTECH.profiles.xlsx", "2016-2020.BTECH.profiles.xlsx", "2017-2021.BTECH.profiles.xlsx"]
const academicYear = "2019-2020"
const sheets = ["CIVIL","EEE","ECE","ME","CSE","IT","PE"]

// for(let i in profiles){
//     for(let j in sheets)
//         converter(profiles[i],sheets[j]);
//     // parseRollNoFromCSV(profiles[i]);
// }
// converter(profiles[1],"Sheet1")

function converter(filename,sheetName){
    console.log(filename)
    const xlsToJson = require('xls-to-json')
    const filepath = './csv/'+filename
    xlsToJson({
        input: filepath,
        output : null,
        sheet:sheetName,
        rowsToSkip: 0
    }, (err, jsonObj) => {
            if (jsonObj) {
            // console.log(jsonObj)
            const course = filename.split('.')[1]
            var n = 0;
            const ROLLNO = {}
            var interval = setInterval(function () {
                var i = n++;
                // console.log(jsonObj[i])
                if (!jsonObj[i]) {
                    console.log('PROCESSING COMPLETED');
                    if (filename.split('.')[0]==="2016-2020") 
                        insertRoolNo(ROLLNO,4);
                    else if (filename.split('.')[0] === "2017-2021")
                        insertRoolNo(ROLLNO, 3);
                    else if (filename.split('.')[0] === "2018-2022")
                        insertRoolNo(ROLLNO,2);
                    clearInterval(interval);
                    return;
                }
                var data = {}
                data = jsonObj[i]
                // console.log(data)
                if (!data.Branch && !data.Section && !data.uid)
                    return;
                var buffer = getBranch(data.Branch)
                data.department = buffer.department
                data.Branch = buffer.Branch
                data.uid = jsonObj[i]['Uni.Reg.No'].toUpperCase()
                data.regno = jsonObj[i].uid
                data.Batch = filename.split('.')[0]
                data.Course = course.toUpperCase()
                if (data.Mobile.match(/^[0-9]{10}/)) {
                    data.Mobile = "+91" + data.Mobile
                    data['Mobile - 1'] = data.Mobile
                    data['Mobile1'] = data.Mobile
                }
                if (data['Mobile - 2'].match(/^[0-9]{10}/)) {
                    data['Mobile - 2'] = "+91" + data['Mobile - 2']
                    data.Mobile2 = data['Mobile - 2']
                }
                data.Program = data.Course
                data.Alumni = false
                if (!data.Section)
                    if(data.section)
                        data.Section = data.section
                    else data.Section = "Sec -"
                if(data.Section.length === 1){
                    data.Section = "Sec "+data.Section.toUpperCase()
                }
                data.Designation = "Student"
                data.role= "STUDENT"
                const collection = "PROFILES"
                if (!ROLLNO[data.Program]) ROLLNO[data.Program] = {}
                if (!ROLLNO[data.Program][data.department]) ROLLNO[data.Program][data.department] = {}
                if (!ROLLNO[data.Program][data.department][data.Section.split(' ')[1]]) ROLLNO[data.Program][data.department][data.Section.split(' ')[1]] = {}
                ROLLNO[data.Program][data.department][data.Section.split(' ')[1]][data.uid] = db.collection('PROFILES').doc(data.uid);
                const document = data.uid
                // console.log(data.uid)
                var docRef = db.collection(collection).doc(document);
                // console.log(data);
                docRef.set(data).then(snapshot =>{
                    if(snapshot.exists)
                        console.log(snapshot.data().uid);
                    else
                        console.log(data.uid + " <==")
                    // insertRoolNo(data)
                }).catch(err => {
                    console.log(err)
                })
                // clearInterval(interval);
            }, 150);
        }
        else {
            console.log("Conversion " + err)
            console.log('failed - ' + filename)
            return;
        }
        console.log('PROCESSING...')
    })
}
function customProfile(){
    const profile = {
        Name: "P KIRAN KUMAR",
        Email: "kiran@sasi.ac.in",
        uid: "kiran",
        Branch : "Computer Science and Engineering",
        department : "CSE",
        Designation : "Associate Professor",
        role:"FACULTY"
    }
    admin.firestore().collection("PROFILES").doc(profile.uid).set(profile,{merge:true}).catch(err => {
        console.log(err)
    })
}
customProfile()
function customProfileUpdate(uid) {
    admin.auth().updateUser(uid,{
        Name: "CSE HOD",
        Email: "hodcse@sasi.ac.in",
        password: "Sasi@123",
        department : "CSE",
        role : "HOD"
    }).catch(err => {
        console.log(err)
    })
}
// customProfileUpdate("hod.cse")

function insertRoolNo(data,year){
    // console.log(data)
    // return;
    var n = 0;
    for(let i in data){
        for(let j in data[i]){
            const input = {}
            for(let k in data[i][j]){
                // console.log(i,j,k,data[i][j][k]);
                // console.log('ROLLNO/' + i + '/' + j + "/" + k);
                input[k] = data[i][j][k]
            }
            for(let k in input){
                const collection = "ACADEMICS/"+academicYear+"/"+i+'/'+j+'/'+year
                console.log(collection,k)
                var docRef = db.collection(collection).doc(k);
                // console.log(data);
                docRef.set(input[k],{merge:true}).then(res => {
                    console.log(res);
                }).catch(err =>
                    console.log(err)
                )
            }
        }
    }
}

function getBranch(dept){
    if(dept === "CSE")
        return { department: "CSE", Branch: "Computer Science and Engineering" }
    if (dept === "ECE")
        return { department: "ECE", Branch: "Electronics and Communication Engineering" }
    if (dept === "EEE")
        return { department: "EEE", Branch: "Electrical and Electronics Engineering" }
    if (dept === "CIVIL" || dept === "CE")
        return { department: "CE", Branch: "Civil Engineering" }
    if (dept === "MECH" || dept === "ME")
        return { department: "ME", Branch: "Mechinal Engineering" }
    if (dept === "PE")
        return { department: "PE", Branch: "Petroleum Engineering" }
    if (dept === "IT")
        return { department: "IT", Branch: "Information Technology" }
    return {department : "---",Branch: "---"}
}

function parseRollNoFromCSV(filename) {
    const xlsToJson = require('xls-to-json')
    const filepath = './csv/' + filename
    xlsToJson({
        input: filepath,
        output: './csv/' + filename + ".json",
        rowsToSkip: 0,
    }, (err, jsonObj) => {
        if (jsonObj) {
            const course = filename.split('.')[1]
            var n = 0;
            const ROLLNO = {}
            var interval = setInterval(function () {
                var i = n++;
                if (!jsonObj[i]) {
                    console.log('PROCESSING COMPLETED');
                    insertRoolNo(ROLLNO);
                    clearInterval(interval);
                    return;
                }
                var data = {}
                data = jsonObj[i]

                if (!data.Branch && !data.Section && !data.uid)
                    return;
                var buffer = getBranch(data.Branch)
                data.department = buffer.department
                data.Branch = buffer.Branch
                data.uid = jsonObj[i]['Uni.Reg.No'].toUpperCase()
                data.regno = jsonObj[i].uid
                data.Batch = filename.split('.')[0]
                data.Course = course.toUpperCase()
                if (data.Mobile.match(/^[0-9]{10}/)) data.Mobile = "+91" + data.Mobile
                if (data['Mobile - 2'].match(/^[0-9]{10}/)) data.Mobile2 = "+91" + data.Mobile2
                data.Program = data.Course
                if (!data.Section)
                    data.Section = "Sec -"
                if (!ROLLNO[data.Program]) ROLLNO[data.Program] = {}
                if (!ROLLNO[data.Program][data.Branch]) ROLLNO[data.Program][data.Branch] = {}
                if (!ROLLNO[data.Program][data.Branch][data.Section.split(' ')[1]]) ROLLNO[data.Program][data.Branch][data.Section.split(' ')[1]] = {}
                ROLLNO[data.Program][data.Branch][data.Section.split(' ')[1]][data.uid] = db.collection('PROFILES').doc(data.uid);
                // insertRoolNo(ROLLNO);
                // clearInterval(interval)
                // console.log(ROLLNO)
            },10);
        }
    })
    
}